var contextPath = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));

$(document).ready(function () {
        $("#searchAutocomplete").autocomplete({
            source: function (request, response) {
                $.get(contextPath + '/searchAutocompleteHandler', {searchPattern: request.term}, function (data) {
                    response($.map(data, function (elementFromData) {
                        return {
                            label: elementFromData.firstName + ' ' + elementFromData.lastName,
                            accountId: elementFromData.id,
                            avatar: elementFromData.avatar
                        }
                    }));
                });
            },
            minLength: 1,
            appendTo: "#myHeader",
            select: function (event, ui) {
                location.href = contextPath + "/id" + ui.item.accountId;
                $(this).val('');
                return false;
            }
        })
            .data("ui-autocomplete")._renderItem = function (ul, item) {
            var $li = $('<li>'),
                $div = $('<div>'),
                $img = $('<img>');
            $div.attr({
                class: "ui-menu-item-wrapper"
            });
            $img.attr({
                class: "autocomplete-avatar-icon",
                src: item.avatar,
                alt: item.label
            });
            $li.append($div);
            $li.find('div').append($img).append("  ").append(item.label);
            return $li.appendTo(ul);
        };
    }
);