var idGenerated = 0;
var workPhonesQuantity = 0;
var personalPhonesQuantity = 0;
var maxPhonesQuantity = 4;

function incrementPhoneId() {
    idGenerated++;
}

function incrementWorkPhonesQuantity() {
    workPhonesQuantity++;
}

function decrementWorkPhonesQuantity() {
    workPhonesQuantity--;
}

function incrementPersonalPhonesQuantity() {
    personalPhonesQuantity++;
}

function decrementPersonalPhonesQuantity() {
    personalPhonesQuantity--;
}

function isValidPhonesQuantity(type) {
    if (type === 'work') {
        return workPhonesQuantity < maxPhonesQuantity;
    } else {
        if (type === 'personal') {
            return personalPhonesQuantity < maxPhonesQuantity;
        } else {
            alert("Wrong phone number type: Expected 'work' or 'personal', actual: " + type)
        }
    }
    return false;
}

function addPhoneField(destinationContainerName, type, inputFieldName) {
    var regExp = /^$/; // check only for empty string (format checked by mask)
    var newPhoneInputField = document.getElementById(inputFieldName);
    var newPhone = newPhoneInputField.value;

    if (isValidPhonesQuantity(type)) {
        if (newPhone.match(regExp)) {
            outputDialog("Phone not entered");
        } else {
            if (type === 'work') {
                incrementWorkPhonesQuantity();
            }
            if (type === 'personal') {
                incrementPersonalPhonesQuantity();
            }
            incrementPhoneId();
            createPhoneField(destinationContainerName, type, newPhone);
            newPhoneInputField.value = "";
        }
    } else {
        newPhoneInputField.value = "";
        outputDialog("Too much phones. Maximum quantity for this group: " + maxPhonesQuantity);
    }
}

function createPhoneField(destinationContainerName, type, phone) {
    var mainDiv = document.createElement('div');
    var inputDiv = document.createElement('div');
    var inputField = document.createElement("input");
    var removeFieldButton = document.createElement("span");

    mainDiv.setAttribute("class", "phone-group");
    mainDiv.setAttribute("id", "id_" + idGenerated);
    inputDiv.setAttribute("class", "col-sm-offset-3 col-sm-8");
    inputField.setAttribute("class", "phoneNumberField form-control input-sm");
    inputField.setAttribute("type", "text");

    if (type === "personal") {
        inputField.setAttribute("name", "personalPhones");
    }
    if (type === "work") {
        inputField.setAttribute("name", "workPhones");
    }
    inputField.value = phone;

    removeFieldButton.setAttribute("class", "deleteFieldButton btn btn-sm btn-link glyphicon glyphicon-remove");
    removeFieldButton.setAttribute("onclick", "removePhoneField('" + destinationContainerName + "','id_" +
        idGenerated + "','" + type + "')");

    mainDiv.appendChild(inputDiv);
    inputDiv.appendChild(inputField);
    mainDiv.appendChild(removeFieldButton);
    document.getElementById(destinationContainerName).appendChild(mainDiv);
}

function removePhoneField(parentDiv, childDiv, type) {
    var child = document.getElementById(childDiv);
    if (child) {
        var parent = document.getElementById(parentDiv);
        parent.removeChild(child);
        if (type === 'work') {
            decrementWorkPhonesQuantity();
        }
        if (type === 'personal') {
            decrementPersonalPhonesQuantity();
        }
    }
}

// jQuery UI Dialog with parametrized text
function outputDialog(dialogText) {
    $(document).ready(function () {
        var textHolder = $('#dialogParametrizedText');
        textHolder.text(dialogText);
        $(textHolder).dialog({
            resizable: false,
            width: 250,
            height: "auto",
            autoOpen: true,
            modal: true,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                }
            },
            open: function (event, ui) {
                $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            }
        });
    });
}

// jQuery UI Dialog for submit changes
function confirmInput() {
    $(document).ready(function () {
        var accountData = $("#accountMainForm");
        var confirmationText = $('#dialogConfirmationText');

        if (!accountData[0].checkValidity()) {  //check for required fields and point to them if not filled
            accountData.find('[type="submit"]').trigger('click');
        } else {
            confirmationText.dialog({
                resizable: false,
                width: 275,
                height: "auto",
                modal: true,
                buttons: {
                    "Save": function () {
                        accountData.submit();
                    },
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                },
                open: function (event, ui) {
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                }
            });
        }
    });
}

// Phones validation mask
$(document).on('focusin', '.phoneNumberField', function () {
    $(function () {
        $(".phoneNumberField").mask("+7(999)999-99-99");
    });
});

// Birthday validation mask
$(function () {
    $("#bday").mask("9999-99-99");
});

// jQuery UI Calendar
$(document).ready(function () {
    $("#bday").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:2017",
        dateFormat: "yy-mm-dd",
        showAnim: "blind"
    });
});

function post(url, name, value) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", url);
    var input = document.createElement("input");
    input.setAttribute("type", "hidden");
    input.setAttribute("name", name);
    input.setAttribute("value", value);
    form.appendChild(input);
    document.body.appendChild(form);
    form.submit();
}