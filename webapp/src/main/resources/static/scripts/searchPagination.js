var contextPath = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));

$(document).ready(function () {
    var searchQuery = $("#query").html();
    var accountsTotal = $("#accountsFound").html();
    var accountsPerPage = 5;
    var currentPage = 0;
    var totalPages = parseInt(accountsTotal / accountsPerPage);
    if (accountsTotal % accountsPerPage !== 0) {
        totalPages++;
    }

    $("#nextPageButton").on("click", function () {
        currentPage++;
        manageButtons();
        $("#pageStatus").html("Page " + currentPage + " of " + (totalPages === 0 ? 1 : totalPages));
        $.get(contextPath + '/searchPaginationHandler', {
            query: searchQuery,
            page: currentPage,
            rows: accountsPerPage
        }, function (data) {
            createPageRows(data)
        })
    });

    $("#prevPageButton").on("click", function () {
        currentPage--;
        manageButtons();
        $("#pageStatus").html("Page " + currentPage + " of " + totalPages);
        $.get(contextPath + '/searchPaginationHandler', {
            query: searchQuery,
            page: currentPage,
            rows: accountsPerPage
        }, function (data) {
            createPageRows(data)
        })
    });

    function createPageRows(data) {
        clearCurrentPage();
        var template = $("#templateRow");
        $.each(data, function (index, account) {
            var newRow = template.clone().removeAttr("id").attr("style", "display:inline");
            var link = newRow.find("a").attr("href", contextPath + "id" + account.id);
            var avatar = newRow.find("img").attr("src", account.avatar);
            link.append(avatar).append(account.firstName + " " + account.lastName);
            $("#accountDataBlock").append(newRow);
        })
    }

    function clearCurrentPage() {
        $("#accountDataBlock").children(".entity-element").each(function () {
            if (!$(this).is("#templateRow")) {
                $(this).remove();
            }
        });
    }

    function manageButtons() {
        if (currentPage < 2) {
            $("#prevPageButton").hide();
        } else {
            $("#prevPageButton").show();
        }
        if (currentPage >= totalPages) {
            $("#nextPageButton").hide();
        } else {
            $("#nextPageButton").show();
        }
    }

    if (currentPage === 0) {
        $('#nextPageButton').click();
    }

});