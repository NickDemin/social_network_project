package com.getjavajob.training.web1701.dyominn.webapp.security;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder.getAccountAvatarString;
import static com.getjavajob.training.web1701.dyominn.webapp.utils.SecurityUtils.getAuthenticatedAccount;

@Component
public class SuccessHandlerImpl extends SavedRequestAwareAuthenticationSuccessHandler {
    private static final Logger logger = LoggerFactory.getLogger(SuccessHandlerImpl.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        logger.info("Authentication is successful. Try to put authenticated account instance to session");
        super.onAuthenticationSuccess(request, response, authentication);
        HttpSession session = request.getSession();
        Account authenticatedAccount = getAuthenticatedAccount();
        if (authenticatedAccount != null) {
            session.setAttribute("authorizedAccount", authenticatedAccount);
            session.setAttribute("authorizedAccountAvatar", getAccountAvatarString(authenticatedAccount.getAvatar(), request.getContextPath()));
            logger.info("Authenticated account instance successfully added to session");
        }
    }
}