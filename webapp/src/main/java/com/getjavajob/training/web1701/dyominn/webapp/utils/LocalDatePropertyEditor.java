package com.getjavajob.training.web1701.dyominn.webapp.utils;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;

public class LocalDatePropertyEditor extends PropertyEditorSupport {

    public LocalDatePropertyEditor() {
    }

    @Override
    public void setAsText(String text) {
        if (text != null && !text.isEmpty()) {
            super.setValue(LocalDate.parse(text));
        } else {
            super.setValue(null);
        }
    }
}