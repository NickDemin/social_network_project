package com.getjavajob.training.web1701.dyominn.webapp.utils;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import java.io.IOException;

public class NonEmptyByteArrayMultipartFileEditor extends ByteArrayMultipartFileEditor {

    @Override
    public void setValue(Object value) {
        if (value instanceof MultipartFile) {
            MultipartFile multipartFile = (MultipartFile) value;
            try {
                byte[] bytes = multipartFile.getBytes();
                super.setValue(bytes.length != 0 ? bytes : null);
            } catch (IOException ex) {
                throw new IllegalArgumentException("Cannot read contents of multipart file", ex);
            }
        } else {
            if (value instanceof byte[]) {
                byte[] bytes = (byte[]) value;
                super.setValue(bytes.length != 0 ? bytes : null);
            } else {
                if (value != null) {
                    byte[] bytes = value.toString().getBytes();
                    super.setValue(bytes.length != 0 ? bytes : null);
                }
            }
        }
    }

    @Override
    public String getAsText() {
        byte[] value = (byte[]) getValue();
        return (value != null ? new String(value) : "");
    }
}