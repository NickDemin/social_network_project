package com.getjavajob.training.web1701.dyominn.webapp.controllers;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

import static com.getjavajob.training.web1701.dyominn.webapp.utils.SecurityUtils.getAuthenticatedAccount;
import static java.lang.Boolean.TRUE;

@Controller
public class AuthenticationController {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

    @GetMapping({"/login", "/"})
    public String login(HttpSession session,
                        String error) {
        logger.info("Received a request to login/start page");
        Account authenticatedAccount = getAuthenticatedAccount();
        if (authenticatedAccount != null) {
            logger.debug("Already authenticated. Redirect to account's page");
            return "redirect:/id" + authenticatedAccount.getId();
        }
        if (error != null) {
            logger.debug("Request to login page has error parameter. Put attribute to session");
            session.setAttribute("loginFailed", TRUE);
            logger.info("Authentication is failed. Redirect to login page");
            return "redirect:/login";
        }
        logger.info("Not authenticated. Forward to login page");
        return "/login";
    }
}