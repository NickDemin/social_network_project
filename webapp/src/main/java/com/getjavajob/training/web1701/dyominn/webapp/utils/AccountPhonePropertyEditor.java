package com.getjavajob.training.web1701.dyominn.webapp.utils;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.getjavajob.training.web1701.dyominn.common.models.PhoneType;

import java.beans.PropertyEditorSupport;

public class AccountPhonePropertyEditor extends PropertyEditorSupport {
    private static final Account DUMMY_OWNER = new Account();
    private PhoneType phoneType;

    public AccountPhonePropertyEditor() {
    }

    public AccountPhonePropertyEditor(PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    @Override
    public void setAsText(String text) {
        if (text != null) {
            Phone phone = new Phone(0, DUMMY_OWNER, text, phoneType);
            super.setValue(phone);
        } else {
            super.setValue(null);
        }
    }
}