package com.getjavajob.training.web1701.dyominn.webapp.dto;

public class AccountDTO {
    private long id;
    private String lastName;
    private String firstName;
    private String avatar;

    public AccountDTO() {
    }

    public long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}