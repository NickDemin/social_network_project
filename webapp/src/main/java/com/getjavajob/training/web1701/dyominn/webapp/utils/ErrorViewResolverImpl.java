package com.getjavajob.training.web1701.dyominn.webapp.utils;

import org.springframework.boot.autoconfigure.web.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Component
public class ErrorViewResolverImpl implements ErrorViewResolver {

    @Override
    public ModelAndView resolveErrorView(HttpServletRequest httpServletRequest, HttpStatus httpStatus, Map<String, Object> map) {
        if (httpStatus == NOT_FOUND) {
            return new ModelAndView("error404");
        } else {
            return new ModelAndView("error");
        }
    }
}