package com.getjavajob.training.web1701.dyominn.webapp.dto;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public final class DTOAssembler {
    private static final Logger logger = LoggerFactory.getLogger(DTOAssembler.class);

    private DTOAssembler() {
    }

    public static AccountDTO convertToDto(Account source, String contextPath) {
        logger.info("Start converting account model to account DTO");
        AccountDTO dto = new AccountDTO();
        dto.setId(source.getId());
        dto.setFirstName(source.getFirstName());
        dto.setLastName(source.getLastName());
        dto.setAvatar(ImageBytesEncoder.getAccountAvatarString(source.getAvatar(), contextPath));
        logger.info("Return account DTO");
        return dto;
    }

    public static List<AccountDTO> convertToDtoList(List<Account> source, String contextPath) {
        logger.info("Start converting accounts list to accounts DTO list");
        List<AccountDTO> dtoList = new ArrayList<>();
        if (source != null) {
            for (Account account : source) {
                AccountDTO dto = convertToDto(account, contextPath);
                dtoList.add(dto);
            }
        }
        logger.info("Return account DTO list");
        return dtoList;
    }
}