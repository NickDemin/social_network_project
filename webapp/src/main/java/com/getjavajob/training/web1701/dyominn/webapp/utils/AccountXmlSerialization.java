package com.getjavajob.training.web1701.dyominn.webapp.utils;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.hibernate.converter.*;
import com.thoughtworks.xstream.hibernate.mapper.HibernateMapper;
import com.thoughtworks.xstream.mapper.MapperWrapper;
import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.StringReader;

import static java.lang.Thread.currentThread;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class AccountXmlSerialization {
    private static final String SCHEMA_NAME = "accountSchema.xsd";

    public static boolean accountXmlIsValid(String xmlAsString) {
        try {
            Source xmlFileSource = new StreamSource(new StringReader(xmlAsString));
            Source xsdFileSource = new StreamSource(currentThread().getContextClassLoader().getResourceAsStream(SCHEMA_NAME));
            SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(xsdFileSource);
            Validator validator = schema.newValidator();
            validator.validate(xmlFileSource);
            return true;
        } catch (SAXException | IOException e) {
            return false;
        }
    }

    public static XStream getXstreamForAccount() {
        XStream xStream = new XStream() {
            protected MapperWrapper wrapMapper(final MapperWrapper next) {
                return new HibernateMapper(next);
            }
        };
        //Hibernate converters
        xStream.registerConverter(new HibernateProxyConverter());
        xStream.registerConverter(new HibernatePersistentCollectionConverter(xStream.getMapper()));
        xStream.registerConverter(new HibernatePersistentMapConverter(xStream.getMapper()));
        xStream.registerConverter(new HibernatePersistentSortedMapConverter(xStream.getMapper()));
        xStream.registerConverter(new HibernatePersistentSortedSetConverter(xStream.getMapper()));
        //Config for Account entity
        xStream.processAnnotations(Account.class);
        xStream.processAnnotations(Phone.class);
        XStream.setupDefaultSecurity(xStream);
        xStream.allowTypesByWildcard(new String[]{Account.class.getPackage().getName() + ".*"});
        return xStream;
    }
}