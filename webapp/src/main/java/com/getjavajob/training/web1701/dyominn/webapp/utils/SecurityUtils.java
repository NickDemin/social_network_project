package com.getjavajob.training.web1701.dyominn.webapp.utils;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public SecurityUtils() {
    }

    public static Account getAuthenticatedAccount() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof AnonymousAuthenticationToken ? null : (Account) authentication.getPrincipal();
    }
}