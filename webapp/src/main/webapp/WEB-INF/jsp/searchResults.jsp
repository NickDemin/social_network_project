<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="searchQuery" value="${requestScope.searchQuery}"/>
<c:set var="accountsQuantityFound" value="${requestScope.accountsQuantityFound}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Search results</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/entityLists.css">
    <link rel="stylesheet" href="${root}/css/searchResults.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <%--Autocomplete's dependencies--%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
    <script src="${root}/scripts/searchPagination.js"></script>
</head>

<body>
<%@ include file="header.jsp" %>

<div class="row" style="padding-top: 5em;">
    <div class="col-sm-offset-3 col-sm-6 well" id="accountDataBlock">
        <div style="text-align: center">Accounts:
            <span id="accountsFound">${accountsQuantityFound}</span>
            <p class="hide-text" id="query">${searchQuery}</p> <!--needed by script for paginate ajax search-->
        </div>
        <div id="templateRow" class="col-sm-offset-2 col-sm-8 entity-element" style="display:none;">
            <div class="row col-sm-11 entity-subelement">
                <a> <img class="entity-avatar" alt="Avatar" src=""> </a>
            </div>
        </div>
    </div>
</div>
<div id="paginationButtonBlock">
    <h5 id="pageStatus"></h5>
    <nav aria-label="...">
        <ul class="pager">
            <li id="prevPageButton">
                <a href="#">
                    <span aria-hidden="true">&larr;</span> Back
                </a>
            </li>
            <li id="nextPageButton">
                <a href="#">Next
                    <span aria-hidden="true"> &rarr;</span>
                </a>
            </li>
        </ul>
    </nav>
</div>
</body>
</html>