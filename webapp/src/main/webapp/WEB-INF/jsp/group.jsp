<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="currentGroup" value="${sessionScope.currentGroup}"/>
<c:set var="currentGroupAvatar" value="${sessionScope.currentGroupAvatar}"/>
<c:set var="isCreator" value="${requestScope.isCreator}"/>
<c:set var="isModerator" value="${requestScope.isModerator}"/>
<c:set var="isMember" value="${requestScope.isMember}"/>
<c:set var="isRequester" value="${requestScope.isRequester}"/>
<c:set var="isNotRelatedUser" value="${!isMember && !isCreator && !isRequester}"/>

<!DOCTYPE html>
<html>
<head>
    <title>${currentGroup.name}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/groupPage.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <%--Autocomplete's dependencies--%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
</head>

<body>
<%@ include file="header.jsp" %>

<div class="col-sm-offset-2 col-sm-10 container text-center" id="groupPageContainer">
    <div class="row">

        <div class="col-sm-6 well" id="groupPageInfoAndWall">
            <div class="col-sm-12 container-fluid" style="margin-top: 0.1em;">
                <h4 class="groupName">${currentGroup.name}</h4>
                <div class="container-fluid" id="groupPageMainInformation">
                    <table>
                        <tr>
                            <td class="groupInfoHeader">Information:</td>
                            <td class="groupInfoData">${currentGroup.description}</td>
                        </tr>
                    </table>
                    <hr/>
                </div>
                <c:if test="${isMember}">
                    <div class="form-group col-sm-12">
                        <textarea class="form-control" rows="1" id="groupWallTextArea"
                                  placeholder="Add a post..."></textarea>
                        <button class="pull-right btn btn-sm btn-success" type="button" id="postBtn">Post</button>
                        <div>
                            <h1 id="groupWall">Group wall</h1>
                        </div>
                    </div>
                </c:if>
                <c:if test="${!isMember}">
                    <h2 id="groupClosedWall">Join the group to view the wall</h2>
                </c:if>
            </div>
        </div>

        <div class="col-sm-3 well">
            <div class="container-fluid" style="margin-top: 0.73em;">
                <img src="${currentGroupAvatar}" class="img-responsive" alt="Avatar" id="mainAvatar">
            </div>
            <div class="container-fluid">
                <ul class="nav nav-pills nav-stacked" id="groupMenu">
                    <li>
                        <a href="${root}/members?group=${currentGroup.id}">
                            <span class="glyphicon glyphicon-user"></span>
                            Members
                        </a>
                    </li>
                    <c:if test="${isNotRelatedUser}">
                        <li>
                            <a href="${root}/sendGroupRequest?id=${currentGroup.id}">
                                <span class="glyphicon glyphicon-bell"></span>
                                Send a request
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${isRequester}">
                        <li>
                            <a href="${root}/cancelGroupRequest?id=${currentGroup.id}">
                                <span class="glyphicon glyphicon-minus-sign"></span>
                                Cancel request
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${isModerator}">
                        <li>
                            <a href="${root}/editGroup">
                                <span class="glyphicon glyphicon-edit"></span>
                                Edit group
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${!isCreator && isMember}">
                        <li>
                            <a href="${root}/leaveGroup?id=${currentGroup.id}">
                                <span class="glyphicon glyphicon-remove-sign"></span>
                                Leave group
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${isCreator}">
                        <li>
                            <a href="${root}/delete?group=${currentGroup.id}">
                                <span class="glyphicon glyphicon-remove"></span>
                                Delete group
                            </a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>