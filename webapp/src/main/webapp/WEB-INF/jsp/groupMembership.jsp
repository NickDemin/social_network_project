<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="imageEncoder" class="com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder"/>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="isModerator" value="${requestScope.isModerator}"/>
<c:set var="moderators" value="${requestScope.moderators}"/>
<c:set var="subscribers" value="${requestScope.subscribers}"/>
<c:set var="requesters" value="${requestScope.requesters}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Members</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/entityLists.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <%--Autocomplete's dependencies--%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div id="entityListMainForm">

    <ul class="col-sm-offset-2 col-sm-2 well" id="entity-list-menu">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a data-toggle="tab" href="#allMembers">All members</a></li>
            <c:if test="${isModerator}">
                <li><a data-toggle="tab" href="#incomingRequests">Incoming requests</a></li>
            </c:if>
        </ul>
    </ul>

    <div class="col-sm-5 well">
        <div class="tab-content">
            <div id="allMembers" class="tab-pane fade in active">
                <c:forEach items="${subscribers}" var="account">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-9 entity-subelement">
                            <a href="${root}/id${account.id}">
                                <c:set var="avatar"
                                       value="${imageEncoder.getAccountAvatarString(account.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">
                                    ${account.firstName} ${account.lastName}
                            </a>
                            <label class="label label-info pull-right entity-label">User</label>
                        </div>
                        <c:if test="${isModerator}">
                            <div class="btn-group pull-right col-sm-4 acceptAndDeclineButton">
                                <a href="${root}/promoteSubscriber?id=${account.id}"
                                   class="btn btn-sm btn-link sub-button" type="submit">Promote
                                </a>
                                <a href="${root}/removeSubscriber?id=${account.id}"
                                   class="btn btn-sm btn-danger sub-button" type="submit">Remove
                                </a>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
                <c:forEach items="${moderators}" var="account">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-9 entity-subelement">
                            <a href="${root}/id${account.id}">
                                <c:set var="avatar"
                                       value="${imageEncoder.getAccountAvatarString(account.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">
                                    ${account.firstName} ${account.lastName}
                            </a>
                            <label class="label label-info pull-right entity-label">Moderator</label>
                        </div>
                        <c:if test="${isModerator}">
                            <div class="btn-group pull-right col-sm-4 acceptAndDeclineButton">
                                <a href="${root}/dismissModerator?id=${account.id}"
                                   class="btn btn-sm btn-link sub-button" type="submit">Dismiss
                                </a>
                                <a href="${root}/removeSubscriber?id=${account.id}"
                                   class="btn btn-sm btn-danger sub-button" type="submit">Remove
                                </a>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
            <div id="incomingRequests" class="tab-pane fade">
                <c:forEach items="${requesters}" var="account">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-9 entity-subelement">
                            <a href="${root}/id${account.id}">
                                <c:set var="avatar"
                                       value="${imageEncoder.getAccountAvatarString(account.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">
                                    ${account.firstName} ${account.lastName}
                            </a>
                            <label class="label label-info pull-right entity-label">Pending</label>
                        </div>
                        <c:if test="${isModerator}">
                            <div class="btn-group pull-right col-sm-4 acceptAndDeclineButton">
                                <a href="${root}/acceptSubscription?id=${account.id}"
                                   class="btn btn-sm btn-success sub-button" type="submit">Accept
                                </a>
                                <a href="${root}/declineSubscription?id=${account.id}"
                                   class="btn btn-sm btn-danger sub-button" type="submit">Decline
                                </a>
                            </div>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>