<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="authorizedAccount" value="${sessionScope.authorizedAccount}"/>
<c:set var="root" value="${pageContext.request.contextPath}"/>

<nav class="navbar navbar-light navbar-fixed-top" id="myHeader">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="${root}/login"><img src="${root}/images/logo.png" class="pull-left" id="headerIcon"/></a>
        </div>

        <c:if test="${authorizedAccount != null}">
            <div class="dropdown navbar-right" id="headerDropDown">
                <button class="btn btn-link dropdown-toggle" type="button" data-toggle="dropdown"
                        id="headerDropDownButton">${authorizedAccount.firstName}${" "}${authorizedAccount.lastName}
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="${root}/id${authorizedAccount.id}">
                            <span class="glyphicon glyphicon-user"></span> My page
                        </a>
                    </li>
                    <li>
                        <a href="${root}/editAccount">
                            <span class="glyphicon glyphicon-edit"></span> Edit profile
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <form action="${root}/logout" method="post" class="inline">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <button type="submit" id="logout-button" class="text-left">
                                <span class="glyphicon glyphicon-off"></span> Log out
                            </button>
                        </form>
                    </li>
                </ul>
            </div>

            <div class="collapse navbar-collapse navbar-left" style="margin-left: 9.92em;">
                <form class="navbar-form" role="search" action="${root}/search" target="_self">
                    <input type="text" class="form-control" name="query" id="searchAutocomplete"
                           placeholder="Search">
                </form>
            </div>
        </c:if>
    </div>
</nav>