<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="loginFailed" value="${sessionScope.loginFailed}"/>
<c:set var="successfullyRegistered" value="${sessionScope.successfullyRegistered}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/login.css">
</head>

<body>
<%@ include file="header.jsp" %>

<div class="container" id="loginBlock">
    <form class="form-signin well" id="loginInner" action="${root}/login" method="post"
          target="_self">

        <div id="loginAndRegistrationStatus">
            <c:if test="${loginFailed}">
                <p id="loginFailedText"><strong>Login failed!</strong> Wrong email or password</p>
                ${sessionScope.remove("loginFailed")}
            </c:if>
            <c:if test="${successfullyRegistered}">
                <p id="registeredText"><strong>Successfully registered!</strong></p>
                ${sessionScope.remove("successfullyRegistered")}
            </c:if>
        </div>

        <h3 id="loginHeader">Please log in</h3>
        <input class="form-control" type="email" name="email" placeholder="Email address" required autofocus>
        <input class="form-control" type="password" name="password" placeholder="Password" required>
        <div class="checkbox" id="rememberMeBox">
            <label>
                <input type="checkbox" name="rememberMe"> Remember me
            </label>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button class="btn btn-md btn-success btn-block" type="submit">Log in</button>
        <p id="loginBlockFooter">
            Don't registered yet?
            <a href="${root}/registration">Register</a>
        </p>
    </form>
</div>
</body>
</html>