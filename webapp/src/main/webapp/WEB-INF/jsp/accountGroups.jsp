<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="imageEncoder" class="com.getjavajob.training.web1701.dyominn.webapp.utils.ImageBytesEncoder"/>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="authorizedAccount" value="${sessionScope.authorizedAccount}"/>
<c:set var="isMyGroups" value="${requestScope.isMyGroups}"/>
<c:set var="createdGroups" value="${requestScope.createdGroups}"/>
<c:set var="moderatedGroups" value="${requestScope.moderatedGroups}"/>
<c:set var="subscriptions" value="${requestScope.subscriptions}"/>
<c:set var="requestedGroups" value="${requestScope.requestedGroups}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Groups</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/entityLists.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <%--Autocomplete's dependencies--%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div id="entityListMainForm">

    <ul class="col-sm-offset-2 col-sm-2 well" id="entity-list-menu">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a data-toggle="tab" href="#allGroups">All groups</a></li>
            <c:if test="${isMyGroups}">
                <li><a data-toggle="tab" href="#requests">My requests</a></li>
                <li class="divider-line"></li>
                <li><a href="${root}/registerGroup">Create new group</a></li>
            </c:if>
        </ul>
    </ul>

    <div class="col-sm-5 well">
        <div class="tab-content">
            <div id="allGroups" class="tab-pane fade in active">
                <c:forEach items="${createdGroups}" var="group">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-11 entity-subelement">
                            <a href="${root}/group${group.id}">
                                <c:set var="avatar" value="${imageEncoder.getGroupAvatarString(group.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">${group.name}
                            </a>
                            <label class="label label-info pull-right entity-label">Creator</label>
                        </div>
                        <c:if test="${isMyGroups}">
                            <a href="${root}/delete?group=${group.id}"
                               class="btn btn-sm btn-danger pull-right entity-element-single-button" type="button">Delete
                            </a>
                        </c:if>
                    </div>
                </c:forEach>
                <c:forEach items="${moderatedGroups}" var="group">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-11 entity-subelement">
                            <a href="${root}/group${group.id}">
                                <c:set var="avatar" value="${imageEncoder.getGroupAvatarString(group.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">${group.name}
                            </a>
                            <label class="label label-info pull-right entity-label">Moderator</label>
                        </div>
                        <c:if test="${isMyGroups}">
                            <a href="${root}/leaveGroup?id=${group.id}"
                               class="btn btn-sm btn-danger pull-right entity-element-single-button" type="button">Leave
                            </a>
                        </c:if>
                    </div>
                </c:forEach>
                <c:forEach items="${subscriptions}" var="group">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-11 entity-subelement">
                            <a href="${root}/group${group.id}">
                                <c:set var="avatar" value="${imageEncoder.getGroupAvatarString(group.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">${group.name}
                            </a>
                            <label class="label label-info pull-right entity-label">User</label>
                        </div>
                        <c:if test="${isMyGroups}">
                            <a href="${root}/leaveGroup?id=${group.id}"
                               class="btn btn-sm btn-danger pull-right entity-element-single-button" type="button">Leave
                            </a>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
            <div id="requests" class="tab-pane fade">
                <c:forEach items="${requestedGroups}" var="group">
                    <div class="col-sm-12 entity-element">
                        <div class="row col-sm-11 entity-subelement">
                            <a href="${root}/group${group.id}">
                                <c:set var="avatar" value="${imageEncoder.getGroupAvatarString(group.avatar, root)}"/>
                                <img class="entity-avatar" src="${avatar}" alt="Avatar">${group.name}
                            </a>
                            <label class="label label-info pull-right entity-label">Pending</label>
                        </div>
                        <c:if test="${isMyGroups}">
                            <a href="${root}/cancelGroupRequest?id=${group.id}"
                               class="btn btn-sm btn-danger pull-right entity-element-single-button" type="button">Cancel
                            </a>
                        </c:if>
                    </div>
                </c:forEach>
                <div class="col-sm-12 divider-line"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>