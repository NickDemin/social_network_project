<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="authorizedAccount" value="${sessionScope.authorizedAccount}"/>
<c:set var="displayedAccount" value="${requestScope.displayedAccount}"/>
<c:set var="displayedAvatar" value="${requestScope.displayedAvatar}"/>
<c:set var="friendshipRequests" value="${requestScope.friendshipRequests}"/>
<c:set var="isMyPage" value="${requestScope.isMyPage}"/>
<c:set var="isMyFriend" value="${requestScope.isMyFriend}"/>
<c:set var="isMySubscription" value="${requestScope.isMySubscription}"/>
<c:set var="notRelatedAccount" value="${!isMyPage && !isMySubscription && !isMyFriend}"/>

<!DOCTYPE html>
<html>
<head>
    <title>${displayedAccount.firstName} ${displayedAccount.lastName}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/accountPage.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <%--Autocomplete's dependencies--%>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
</head>

<body>
<%@ include file="header.jsp" %>

<div class="col-sm-offset-2 col-sm-10 container text-center" id="accountPageContainer">
    <div class="row">

        <div class="col-sm-3 well">
            <div class="container-fluid" id="avatarContainer">
                <img src="${displayedAvatar}" class="img-responsive" alt="Avatar" id="mainAvatar">
            </div>
            <div class="container-fluid">
                <ul class="nav nav-pills nav-stacked" id="accountMenu">
                    <li>
                        <c:if test="${isMyPage}">
                            <a href="#"> <span class="glyphicon glyphicon-envelope"></span>
                                My messages
                            </a>
                        </c:if>
                        <c:if test="${!isMyPage}">
                            <a href="#"> <span class="glyphicon glyphicon-envelope"></span>
                                Send Message
                            </a>
                        </c:if>
                    </li>
                    <li>
                        <a href="${root}/friends?id=${displayedAccount.id}">
                            <span class="glyphicon glyphicon-user"></span>
                            <c:if test="${isMyPage}"> My friends
                                <c:if test="${friendshipRequests > 0}">
                                    <span class="label label-default pull-right menu-label">+${friendshipRequests}</span>
                                </c:if>
                            </c:if>
                            <c:if test="${!isMyPage}">${displayedAccount.firstName}'s friends</c:if>
                        </a>
                    </li>
                    <li>
                        <a href="${root}/groups?id=${displayedAccount.id}">
                            <span class="glyphicon glyphicon-th-large"></span>
                            <c:if test="${displayedAccount.equals(authorizedAccount)}"> My groups</c:if>
                            <c:if test="${!displayedAccount.equals(authorizedAccount)}"> ${displayedAccount.firstName}'s groups</c:if>
                        </a>
                    </li>
                    <%--Friendship relations buttons' variations--%>
                    <c:if test="${isMyFriend}">
                        <li><a href="${root}/remove?friend=${displayedAccount.id}"> <span
                                class="glyphicon glyphicon-remove-sign"></span> Remove from friends</a></li>
                    </c:if>
                    <c:if test="${isMySubscription}">
                        <li><a href="${root}/cancelOutgoingFriendship?id=${displayedAccount.id}"> <span
                                class="glyphicon glyphicon-minus-sign"></span> Cancel request</a></li>
                    </c:if>
                    <c:if test="${notRelatedAccount}">
                        <li><a href="${root}/offerFriendship?id=${displayedAccount.id}"> <span
                                class="glyphicon glyphicon-plus-sign"></span> Add to friends</a></li>
                    </c:if>
                </ul>
            </div>
        </div>

        <div class="col-sm-6 well" id="accountPageInfoAndWall">
            <div class="col-sm-12 container-fluid">
                <h4 class="accountName">${displayedAccount.firstName} ${displayedAccount.middleName} ${displayedAccount.lastName}</h4>

                <div class="container-fluid" id="accountPageMainInformation">
                    <table>
                        <tr>
                            <td class="accountInfoHeader">Birth date:</td>
                            <td class="accountInfoData">${displayedAccount.birthDate}</td>
                        </tr>
                        <tr>
                            <td class="accountInfoHeader">Home address:</td>
                            <td class="accountInfoData">${displayedAccount.homeAddress}</td>
                        </tr>
                    </table>

                    <button type="button" class="btn btn-default" id="addInfBtn" data-toggle="collapse"
                            data-target="#additionalInfo">
                        Show full information
                    </button>
                </div>

                <div class="collapse out" id="additionalInfo">
                    <div class="container-fluid" id="accountPageAdditionalInformation">
                        <table>
                            <tr>
                                <td class="accountInfoHeader">Work address:</td>
                                <td class="accountInfoData">${displayedAccount.workAddress}</td>
                            </tr>
                            <tr>
                                <td class="accountInfoHeader">E-mail:</td>
                                <td class="accountInfoData">${displayedAccount.email}</td>
                            </tr>
                            <tr>
                                <td class="accountInfoHeader">Icq:</td>
                                <td class="accountInfoData">${displayedAccount.icq}</td>
                            </tr>
                            <tr>
                                <td class="accountInfoHeader">Skype:</td>
                                <td class="accountInfoData">${displayedAccount.skype}</td>
                            </tr>
                            <tr>
                                <td class="accountInfoHeader" style="vertical-align: top;">Personal phones:</td>
                                <td class="accountInfoData">
                                    <table>
                                        <c:forEach var="phone" items="${displayedAccount.personalPhones}">
                                            <tr>
                                                <td>${phone.phoneNumber}</td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="accountInfoHeader" style="vertical-align: top;">Work phones:</td>
                                <td class="accountInfoData">
                                    <table>
                                        <c:forEach var="phone" items="${displayedAccount.workPhones}">
                                            <tr>
                                                <td>${phone.phoneNumber}</td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="accountInfoHeader">Additional information:</td>
                                <td class="accountInfoData">${displayedAccount.additionalInfo}</td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <textarea class="form-control" rows="2" id="wallNewsTextArea" placeholder="What's new?"></textarea>
                    <span>
                        <button class="pull-right btn btn-sm btn-success" type="submit" id="postBtn">Send</button>
                    </span>
                    <div>
                        <h1 id="accountWall">Account wall</h1>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>