<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="authorizedAccount" value="${sessionScope.authorizedAccount}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Group registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/entityData.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="container text-center">
    <div class="row">

        <div class="col-sm-offset-3 col-sm-6 well" id="groupDataBlock">
            <div class="container-fluid">
                <h4>Group registration:</h4>
                <form action="${root}/groupRegistrationHandler?${_csrf.parameterName}=${_csrf.token}" method="post"
                      target="_self" enctype="multipart/form-data">
                    <div class="container-fluid well" id="avatarBlock">
                        <div class="form">
                            <output id="avatarPreview"></output>
                            <label for="avatar" class="btn btn-success btn-sm" style="margin-top: 1em;">
                                Choose avatar
                            </label>
                            <input class="center-block" type="file" name="avatar" id="avatar"
                                   style="visibility:hidden;">
                        </div>
                    </div>
                    <div class="container-fluid well" id="groupBlock">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="groupName">Group name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="name" id="groupName"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="groupDescription">
                                    Description:
                                </label>
                                <div class="col-sm-8">
                                    <textarea class="form-control input-sm" name="description"
                                              id="groupDescription">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-success btn-sm" type="submit" id="commitBtn">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('avatarPreview').innerHTML = ['<img src="${root}/images/group-avatar-placeholder.png" width="200" />'].join('');
</script>
<script src="${root}/scripts/avatarPreview.js"></script>

</body>
</html>