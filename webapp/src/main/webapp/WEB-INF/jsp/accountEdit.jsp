<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="root" value="${pageContext.request.contextPath}"/>
<c:set var="authorizedAccount" value="${sessionScope.authorizedAccount}"/>
<c:set var="authorizedAccountAvatar" value="${sessionScope.authorizedAccountAvatar}"/>
<c:set var="xmlAccount" value="${requestScope.accountFromXml}"/>
<c:set var="account" value="${xmlAccount != null ? xmlAccount : authorizedAccount}"/>
<c:set var="xmlFileException" value="${requestScope.xmlFileException}"/>

<!DOCTYPE html>
<html>
<head>
    <title>Edit profile</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="${root}/css/common.css">
    <link rel="stylesheet" href="${root}/css/header.css">
    <link rel="stylesheet" href="${root}/css/entityData.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="${root}/scripts/jquery.maskedinput.js"></script>
    <script src="${root}/scripts/jquery.maskedinput.min.js"></script>
    <script src="${root}/scripts/searchAutocomplete.js"></script>
    <script src="${root}/scripts/accountData.js"></script>
</head>

<body>
<%@ include file="header.jsp" %>

<div class="container text-center" id="accountPageContainer">
    <div class="row">

        <div class="col-sm-offset-3 col-sm-6 well" id="accountDataBlock">
            <div class="container-fluid">
                <h4>Edit profile:</h4>

                <form action="${root}/editAccountHandler?${_csrf.parameterName}=${_csrf.token}" method="post"
                      target="_self" enctype="multipart/form-data"
                      id="accountMainForm">
                    <div class="container-fluid well" id="avatarBlock">
                        <div class="form">
                            <output id="avatarPreview"></output>
                            <label for="avatar" class="btn btn-success btn-sm" style="margin-top: 1em;">
                                Change photo
                            </label>
                            <input class="center-block" type="file" name="avatar" id="avatar"
                                   style="visibility:hidden;">
                        </div>
                    </div>

                    <div class="container-fluid well" id="personBlock">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="fname">First name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="firstName" id="fname"
                                           required value="${account.firstName}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="lname">Last name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="lastName" id="lname"
                                           required value="${account.lastName}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="mname">Middle name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="middleName" id="mname"
                                           value="${account.middleName}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="bday">Birth date:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="birthDate" id="bday"
                                           placeholder="yyyy-mm-dd" value="${account.birthDate}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="email">E-mail:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="email" name="email" id="email"
                                           required value="${account.email}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="pass">Password:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="password" name="password" id="pass"
                                           placeholder="Fill this field, if password changing is needed">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid well" id="phonesBlock">
                        <div class="form-horizontal">
                            <div class="form-group" id="personalPhonesBlock">
                                <div class="phone-group">
                                    <label class="control-label col-sm-3 dataHeader" for="personalPhoneInput">
                                        Personal phone:
                                    </label>
                                    <div class="col-sm-8">
                                        <input class="phoneNumberField form-control input-sm" type="text"
                                               id="personalPhoneInput" placeholder="+7(***)***-**-**">
                                    </div>
                                    <span class="addFieldButton btn btn-sm btn-link glyphicon glyphicon-plus"
                                          onclick="addPhoneField('personalPhonesBlock', 'personal', 'personalPhoneInput')">
                                    </span>
                                </div>
                                <c:set var="personalPhoneId"
                                       value="-200"/> <%--such value do not cross with js' ids for dynamically-generated fields--%>
                                <c:forEach items="${account.personalPhones}" var="phone">
                                    <c:set var="personalPhoneId" value="${personalPhoneId + 1}"/>
                                    <script>
                                        incrementPersonalPhonesQuantity();
                                    </script>
                                    <div class="phone-group"
                                         id="id_${personalPhoneId}">
                                        <div class="col-sm-offset-3 col-sm-8">
                                            <input class="phoneNumberField form-control input-sm" type="text"
                                                   name="personalPhones" value="${phone.phoneNumber}">
                                        </div>
                                        <span class="deleteFieldButton btn btn-sm btn-link glyphicon glyphicon-remove"
                                              onclick="removePhoneField('personalPhonesBlock', 'id_${personalPhoneId}', 'personal')">
                                        </span>
                                    </div>
                                </c:forEach>
                            </div>

                            <div class="form-group" id="workPhonesBlock">
                                <div class="phone-group">
                                    <label class="control-label col-sm-3 dataHeader" for="workPhoneInput">
                                        Work phone:
                                    </label>
                                    <div class="col-sm-8">
                                        <input class="phoneNumberField form-control input-sm" type="text"
                                               id="workPhoneInput" placeholder="+7(***)***-**-**">
                                    </div>
                                    <span class="addFieldButton btn btn-sm btn-link glyphicon glyphicon-plus"
                                          onclick="addPhoneField('workPhonesBlock', 'work', 'workPhoneInput')"></span>
                                </div>
                                <c:set var="workPhoneId"
                                       value="-100"/> <%--such value do not cross with js' ids for dynamically-generated fields--%>
                                <c:forEach items="${account.workPhones}" var="phone">
                                    <c:set var="workPhoneId" value="${workPhoneId + 1}"/>
                                    <script>
                                        incrementWorkPhonesQuantity();
                                    </script>
                                    <div class="phone-group"
                                         id="id_${workPhoneId}">
                                        <div class="col-sm-offset-3 col-sm-8">
                                            <input class="phoneNumberField form-control input-sm" type="text"
                                                   name="workPhones" value="${phone.phoneNumber}">
                                        </div>
                                        <span class="deleteFieldButton btn btn-sm btn-link glyphicon glyphicon-remove"
                                              onclick="removePhoneField('workPhonesBlock', 'id_${workPhoneId}', 'work')">
                                        </span>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>

                    <div class="container-fluid well" id="contactsBlock">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="hAdrs">Home address:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="homeAddress" id="hAdrs"
                                           value="${account.homeAddress}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="wAdrs">Work address:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="workAddress" id="wAdrs"
                                           value="${account.workAddress}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="icq">ICQ:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="icq" id="icq"
                                           value="${account.icq}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="skype">Skype:</label>
                                <div class="col-sm-8">
                                    <input class="form-control input-sm" type="text" name="skype" id="skype"
                                           value="${account.skype}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 dataHeader" for="addInf">Additional info:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control input-sm" name="additionalInfo"
                                              id="addInf">${account.additionalInfo}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p id="dialogConfirmationText" style="text-align: center;" hidden>
                        Do you really want to save changes?
                    </p>
                    <p id="dialogParametrizedText" style="text-align: center;" hidden></p>


                    <div class="btn-group btn-group-sm">
                        <span class="btn btn-success btn-sm" onclick="confirmInput()" id="commitBtn">
                            Save changes
                        </span>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                Edit with XML
                                <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu">
                                <li class="xmlDropdownBtn">
                                    <a class="btn" href="${root}/saveAccountAsXml">
                                        <span class="glyphicon glyphicon-floppy-save"></span> Save as XML
                                    </a>
                                </li>
                                <li class="xmlDropdownBtn">
                                    <label class="btn" for="xmlAccountData">
                                        <span class="glyphicon glyphicon-download-alt"></span> Load from XML
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <input type="file" id="xmlAccountData" name="xmlAccountData" style="visibility:hidden;"/>

                    <!--jQuery hook for HTML5 required fields check -->
                    <button type="submit" class="hide"></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('avatarPreview').innerHTML = ['<img src="${authorizedAccountAvatar}" width="200" />'].join('');

    $(document).ready(function () {
        if ("${xmlFileException}") {
            outputDialog("${xmlFileException}")
        }
    });

    // xml data upload
    $(document).ready(function () {
        var xmlLoader = document.getElementById("xmlAccountData");
        if (xmlLoader) {
            xmlLoader.addEventListener("change", function (event) {
                var file = event.target.files[0];
                if (!file.type.match("text/xml*")) {
                    outputDialog("Illegal file format. Expected text/xml file");
                    return;
                }
                if (file.size > 1024 * 1024) {
                    outputDialog("File size cannot exceed 1MB");
                    return;
                }
                var reader = new FileReader();
                reader.onload = function (event) {
                    var xmlString = event.target.result;
                    post("${root}/editAccountByXmlHandler?${_csrf.parameterName}=${_csrf.token}", "xmlAccount", xmlString);
                };
                reader.onerror = function (event) {
                    outputDialog("Can't read the file: " + event.target.error.code);
                };
                reader.readAsText(file);
            });
        }
    });
</script>
<script src="${root}/scripts/avatarPreview.js"></script>

</body>
</html>