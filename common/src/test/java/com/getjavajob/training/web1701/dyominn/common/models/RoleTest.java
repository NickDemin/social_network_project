package com.getjavajob.training.web1701.dyominn.common.models;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class RoleTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void setAccountsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as accounts collection");
        Role role = new Role();
        role.setAccounts(null);
    }

    @Test
    public void hashCodeConsistency() {
        Role role = new Role(1, "ROLE_USER");
        int hash = role.hashCode();
        for (int i = 0; i < 128; i++) {
            if (hash != role.hashCode()) {
                fail("hashCode() not consistent");
            }
        }
        assertTrue(role.hashCode() == hash);
    }

    @Test
    public void hashCodeConsistentWithEquals() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(1, "ROLE_USER");
        assertEquals(role1.equals(role2), role1.hashCode() == role2.hashCode());
    }

    @Test
    public void equalsTheSame() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(1, "ROLE_USER");
        assertEquals(role1, role2);
    }

    @Test
    public void equalsDistinct() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(1, "ROLE_ADMIN");
        Role role3 = new Role(2, "ROLE_ADMIN");
        assertNotEquals(role1, role2);
        assertNotEquals(role1, role3);
    }

    @Test
    public void equalsReflexive() {
        Role role = new Role(1, "ROLE_USER");
        //noinspection EqualsWithItself // ok, for testing needs
        assertEquals(role, role);
    }

    @Test
    public void equalsSymmetric() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(1, "ROLE_USER");
        assertEquals(role1.equals(role2), role2.equals(role1));
    }

    @Test
    public void equalsTransitive() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(1, "ROLE_USER");
        Role role3 = new Role(1, "ROLE_USER");
        assertEquals(role1.equals(role2) && role2.equals(role3), role1.equals(role3));
    }

    @Test
    public void equalsConsistencyTrue() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(1, "ROLE_USER");
        for (int i = 0; i < 128; i++) {
            if (!role1.equals(role2)) {
                fail("equals() not consistent");
            }
        }
        assertEquals(role1, role2);
    }

    @Test
    public void equalsConsistencyFalse() {
        Role role1 = new Role(1, "ROLE_USER");
        Role role2 = new Role(2, "ROLE_ADMIN");
        for (int i = 0; i < 128; i++) {
            if (role1.equals(role2)) {
                fail("equals() not consistent");
            }
        }
        assertNotEquals(role1, role2);
    }

    @Test
    public void equalsNonNullity() {
        Role role = new Role(1, "ROLE_USER");
        //noinspection ObjectEqualsNull // ok, for testing needs
        assertNotEquals(role, null);
    }

    @Test
    public void toStringTest() {
        Role role = new Role(1, "ROLE_USER");
        String expected = "Role{id=1, name=ROLE_USER}";
        assertEquals(expected, role.toString());
    }
}