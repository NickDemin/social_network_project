package com.getjavajob.training.web1701.dyominn.common.models;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static java.time.LocalDate.parse;
import static org.junit.Assert.*;

public class GroupTest {
    private final Account admin1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
            "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
            "First user here", "password", null);
    private final Account admin2 = new Account(3, "Petrov", "Petr", null, parse("1980-01-01"), null, null,
            "petrov@gmail.com", null, null, null, "password", null);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void createWithNullName() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Group's name can't be null");
        new Group(1, admin1, null, "Космос в картинках и фактах");
    }

    @Test
    public void createWithNullCreator() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Group's creator can't be null");
        new Group(1, null, "Deep space", "Космос в картинках и фактах");
    }

    @Test
    public void setModeratorsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set group's moderators collection as null");
        Group group = new Group();
        group.setModerators(null);
    }

    @Test
    public void setSubscribersAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set group's subscribers collection as null");
        Group group = new Group();
        group.setSubscribers(null);
    }

    @Test
    public void setRequestersAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set group's requests collection as null");
        Group group = new Group();
        group.setRequesters(null);
    }

    @Test
    public void hashCodeConsistency() {
        Group group = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        int hash = group.hashCode();
        for (int i = 0; i < 128; i++) {
            if (hash != group.hashCode()) {
                fail("hashCode() not consistent");
            }
        }
        assertTrue(group.hashCode() == hash);
    }

    @Test
    public void hashCodeConsistentWithEquals() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        assertEquals(group1.equals(group2), group1.hashCode() == group2.hashCode());
    }

    @Test
    public void equalsTheSame() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        assertEquals(group1, group2);
    }

    @Test
    public void equalsDistinct() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(2, admin2, "Хабрахабр", "НЛО с вами!");
        assertNotEquals(group1, group2);
    }

    @Test
    public void equalsReflexive() {
        Group group = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        //noinspection EqualsWithItself // ok, for testing needs
        assertEquals(group, group);
    }

    @Test
    public void equalsSymmetric() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        assertEquals(group1.equals(group2), group2.equals(group1));
    }

    @Test
    public void equalsTransitive() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group3 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        assertEquals(group1.equals(group2) && group2.equals(group3), group1.equals(group3));
    }

    @Test
    public void equalsConsistencyTrue() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        for (int i = 0; i < 128; i++) {
            if (!group1.equals(group2)) {
                fail("equals() not consistent");
            }
        }
        assertEquals(group1, group2);
    }

    @Test
    public void equalsConsistencyFalse() {
        Group group1 = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        Group group2 = new Group(2, admin2, "Хабрахабр", "НЛО с вами!");
        for (int i = 0; i < 128; i++) {
            if (group1.equals(group2)) {
                fail("equals() not consistent");
            }
        }
        assertNotEquals(group1, group2);
    }

    @Test
    public void equalsNonNullity() {
        Group group = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        //noinspection ObjectEqualsNull // ok, for testing needs
        assertNotEquals(group, null);
    }

    @Test
    public void toStringTest() {
        Group group = new Group(1, admin1, "Deep space", "Космос в картинках и фактах");
        String expected = "Group {id=1, creatorId=1, name=Deep space, description=Космос в картинках и фактах}";
        assertEquals(expected, group.toString());
    }
}