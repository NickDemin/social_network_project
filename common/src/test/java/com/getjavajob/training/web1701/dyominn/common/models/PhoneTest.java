package com.getjavajob.training.web1701.dyominn.common.models;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDate;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static org.junit.Assert.*;

public class PhoneTest {
    private final Account owner = new Account(1, "Ivanov", "Ivan", "Ivanovich", LocalDate.parse("1965-03-15"),
            "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
            "First user here", "123", null);

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void createWithNullOwner() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Phone's owner can't be null");
        new Phone(1, null, "88005553535", HOME);
    }

    @Test
    public void createWithNullPhoneNumber() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Phone's number can't be null");
        new Phone(1, owner, null, HOME);
    }

    @Test
    public void createWithNullType() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Phone's type can't be null");
        new Phone(1, owner, "88005553535", null);
    }

    @Test
    public void hashCodeConsistency() {
        Phone phone = new Phone(1, owner, "88005553535", HOME);
        int hash = phone.hashCode();
        for (int i = 0; i < 128; i++) {
            if (hash != phone.hashCode()) {
                fail("hashCode() not consistent");
            }
        }
        assertTrue(phone.hashCode() == hash);
    }

    @Test
    public void hashCodeConsistentWithEquals() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(1, owner, "88005553535", HOME);
        assertEquals(phone1.equals(phone2), phone1.hashCode() == phone2.hashCode());
    }

    @Test
    public void equalsTheSame() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(1, owner, "88005553535", HOME);
        assertEquals(phone1, phone2);
    }

    @Test
    public void equalsDistinct() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(1, owner, "88005553535", WORK);
        Phone phone3 = new Phone(2, new Account(), "4815162342", WORK);
        assertNotEquals(phone1, phone2);
        assertNotEquals(phone1, phone3);
    }

    @Test
    public void equalsReflexive() {
        Phone phone = new Phone(1, owner, "88005553535", HOME);
        //noinspection EqualsWithItself // ok, for testing needs
        assertEquals(phone, phone);
    }

    @Test
    public void equalsSymmetric() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(1, owner, "88005553535", HOME);
        assertEquals(phone1.equals(phone2), phone2.equals(phone1));
    }

    @Test
    public void equalsTransitive() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(1, owner, "88005553535", HOME);
        Phone phone3 = new Phone(1, owner, "88005553535", HOME);
        assertEquals(phone1.equals(phone2) && phone2.equals(phone3), phone1.equals(phone3));
    }

    @Test
    public void equalsConsistencyTrue() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(1, owner, "88005553535", HOME);
        for (int i = 0; i < 128; i++) {
            if (!phone1.equals(phone2)) {
                fail("equals() not consistent");
            }
        }
        assertEquals(phone1, phone2);
    }

    @Test
    public void equalsConsistencyFalse() {
        Phone phone1 = new Phone(1, owner, "88005553535", HOME);
        Phone phone2 = new Phone(3, new Account(), "123456", WORK);
        for (int i = 0; i < 128; i++) {
            if (phone1.equals(phone2)) {
                fail("equals() not consistent");
            }
        }
        assertNotEquals(phone1, phone2);
    }

    @Test
    public void equalsNonNullity() {
        Phone phone = new Phone(1, owner, "88005553535", HOME);
        //noinspection ObjectEqualsNull // ok, for testing needs
        assertNotEquals(phone, null);
    }

    @Test
    public void toStringTest() {
        Phone phone = new Phone(128, owner, "88005553535", HOME);
        String expected = "Phone{id=128, ownerId=1, phoneNumber=88005553535, type=HOME}";
        assertEquals(expected, phone.toString());
    }
}