package com.getjavajob.training.web1701.dyominn.common.models;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static java.time.LocalDate.parse;
import static org.junit.Assert.*;

public class AccountTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void createWithNullLastName() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Account's last name can't be null");
        new Account(1, null, "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
    }

    @Test
    public void createWithNullFirstName() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Account's first name can't be null");
        new Account(1, "Ivanov", null, "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
    }

    @Test
    public void createWithNullEmail() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Account's email can't be null");
        new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", null, "12345", "ivanSkype",
                "First user here", "123", null);
    }

    @Test
    public void createWithNullPassword() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Account's password can't be null");
        new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", null, null);
    }

    @Test
    public void setFriendsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as friends collection");
        Account account = new Account();
        account.setFriends(null);
    }

    @Test
    public void setPersonalPhonesAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as personal phones collection");
        Account account = new Account();
        account.setPersonalPhones(null);
    }

    @Test
    public void setWorkPhonesAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as work phones collection");
        Account account = new Account();
        account.setWorkPhones(null);
    }

    @Test
    public void setIncomingFriendRequestsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as incoming friendship requests collection");
        Account account = new Account();
        account.setIncomingFriendRequests(null);
    }

    @Test
    public void setOutcomingFriendRequestsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as outcoming friendship requests collection");
        Account account = new Account();
        account.setOutcomingFriendRequests(null);
    }

    @Test
    public void setCreatedGroupsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as created groups collection");
        Account account = new Account();
        account.setCreatedGroups(null);
    }

    @Test
    public void setModeratedGroupsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as moderated groups collection");
        Account account = new Account();
        account.setModeratedGroups(null);
    }

    @Test
    public void setSubscriptionsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as subscriptions to groups collection");
        Account account = new Account();
        account.setSubscriptions(null);
    }

    @Test
    public void setRequestedGroupsAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as requests to groups collection");
        Account account = new Account();
        account.setRequestedGroups(null);
    }

    @Test
    public void setRolesAsNull() {
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Cannot set null as account's roles collection");
        Account account = new Account();
        account.setRoles(null);
    }

    @Test
    public void hashCodeConsistency() {
        Account account = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        int hash = account.hashCode();
        for (int i = 0; i < 128; i++) {
            if (hash != account.hashCode()) {
                fail("hashCode() not consistent");
            }
        }
        assertTrue(account.hashCode() == hash);
    }

    @Test
    public void hashCodeConsistentWithEquals() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        assertEquals(account1.equals(account2), account1.hashCode() == account2.hashCode());
    }

    @Test
    public void equalsTheSame() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        assertEquals(account1, account2);
    }

    @Test
    public void equalsDistinct() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(3, "Petrov", "Petr", null, parse("1980-01-01"), null, null,
                "petrov@gmail.com", null, null, "User with only mandatory fields", "123", null);
        assertNotEquals(account1, account2);
    }

    @Test
    public void equalsReflexive() {
        Account account = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        //noinspection EqualsWithItself // ok, for testing needs
        assertEquals(account, account);
    }

    @Test
    public void equalsSymmetric() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        assertEquals(account1.equals(account2), account2.equals(account1));
    }

    @Test
    public void equalsTransitive() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account3 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        assertEquals(account1.equals(account2) && account2.equals(account3), account1.equals(account3));
    }

    @Test
    public void equalsConsistencyTrue() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        for (int i = 0; i < 128; i++) {
            if (!account1.equals(account2)) {
                fail("equals() not consistent");
            }
        }
        assertEquals(account1, account2);
    }

    @Test
    public void equalsConsistencyFalse() {
        Account account1 = new Account(1, "Ivanov", "Ivan", "Ivanovich", parse("1965-03-15"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "ivanov@gmail.com", "12345", "ivanSkype",
                "First user here", "123", null);
        Account account2 = new Account(3, "Petrov", "Petr", null, parse("1980-01-01"), null, null,
                "petrov@gmail.com", null, null, "User with only mandatory fields", "123", null);
        for (int i = 0; i < 128; i++) {
            if (account1.equals(account2)) {
                fail("equals() not consistent");
            }
        }
        assertNotEquals(account1, account2);
    }

    @Test
    public void equalsNonNullity() {
        Account account = new Account(3, "Petrov", "Petr", null, parse("1980-01-01"), null, null,
                "petrov@gmail.com", null, null, "User with only mandatory fields", "123", null);
        //noinspection ObjectEqualsNull // ok, for testing needs
        assertNotEquals(account, null);
    }

    @Test
    public void toStringTest() {
        Account account = new Account(3, "Petrov", "Petr", null, parse("1980-01-01"), null, null,
                "petrov@gmail.com", null, null, "User with only mandatory fields", "123", null);
        String expected = "Account {id=3, lastName=Petrov, firstName=Petr, middleName=null, birthDate=1980-01-01, " +
                "homeAddress=null, workAddress=null, email=petrov@gmail.com, icq=null, skype=null, " +
                "additionalInfo=User with only mandatory fields}";
        assertEquals(expected, account.toString());
    }
}