package com.getjavajob.training.web1701.dyominn.common.models;

import com.getjavajob.training.web1701.dyominn.common.Entity;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

import static java.util.Objects.requireNonNull;
import static javax.persistence.CascadeType.*;
import static javax.persistence.GenerationType.IDENTITY;

@javax.persistence.Entity
@Table(name = "accounts", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
@XStreamAlias("Account")
public class Account implements Entity, UserDetails, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "user_id")
    @XStreamAsAttribute
    private long id;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String firstName;
    private String middleName;
    private LocalDate birthDate;
    private String homeAddress;
    private String workAddress;
    @Column(unique = true, nullable = false)
    private String email;
    private String icq;
    private String skype;
    private String additionalInfo;
    @Column(nullable = false)
    @XStreamOmitField
    private String password;
    @XStreamOmitField
    private byte[] avatar;
    @OneToMany(mappedBy = "owner", cascade = ALL, orphanRemoval = true)
    @Where(clause = "type = 'WORK'")
    private List<Phone> workPhones = new ArrayList<>();
    @OneToMany(mappedBy = "owner", cascade = ALL, orphanRemoval = true)
    @Where(clause = "type = 'HOME'")
    private List<Phone> personalPhones = new ArrayList<>();

    // Friendship relations mapping
    @ManyToMany
    @JoinTable(
            name = "friends",
            joinColumns = {@JoinColumn(name = "first_account_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "second_account_id", nullable = false)})
    @XStreamOmitField
    private Set<Account> friends = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "requests_to_friends",
            joinColumns = {@JoinColumn(name = "account_to_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "account_from_id", nullable = false)})
    @XStreamOmitField
    private Set<Account> incomingFriendRequests = new HashSet<>();

    @ManyToMany(mappedBy = "incomingFriendRequests")
    @XStreamOmitField
    private Set<Account> outcomingFriendRequests = new HashSet<>();

    // Groups mapping
    @OneToMany(mappedBy = "creator", cascade = {MERGE, REMOVE})
    @XStreamOmitField
    private List<Group> createdGroups = new ArrayList<>();

    @ManyToMany(cascade = MERGE)
    @JoinTable(
            name = "groups_moderators",
            joinColumns = {@JoinColumn(name = "moderator_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "group_id", nullable = false)})
    @XStreamOmitField
    private Set<Group> moderatedGroups = new HashSet<>();

    @ManyToMany(cascade = MERGE)
    @JoinTable(
            name = "groups_subscribers",
            joinColumns = {@JoinColumn(name = "subscriber_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "group_id", nullable = false)})
    @XStreamOmitField
    private Set<Group> subscriptions = new HashSet<>();

    @ManyToMany(cascade = MERGE)
    @JoinTable(
            name = "groups_requests",
            joinColumns = {@JoinColumn(name = "requester_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "group_id", nullable = false)})
    @XStreamOmitField
    private Set<Group> requestedGroups = new HashSet<>();
    @ManyToMany
    @JoinTable(
            name = "accounts_roles",
            joinColumns = {@JoinColumn(name = "account_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "role_id", nullable = false)})
    @XStreamOmitField
    private Set<Role> roles = new HashSet<>();

    public Account() {
    }

    public Account(long id, String lastName, String firstName, String middleName, LocalDate birthDate,
                   String homeAddress, String workAddress, String email, String icq, String skype, String additionalInfo,
                   String password, byte[] avatar) {
        setId(id);
        setLastName(lastName);
        setFirstName(firstName);
        setMiddleName(middleName);
        setBirthDate(birthDate);
        setHomeAddress(homeAddress);
        setWorkAddress(workAddress);
        setEmail(email);
        setIcq(icq);
        setSkype(skype);
        setAdditionalInfo(additionalInfo);
        setPassword(password);
        setAvatar(avatar);
    }

    @Override
    public long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public List<Phone> getPersonalPhones() {
        return personalPhones;
    }

    public List<Phone> getWorkPhones() {
        return workPhones;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public String getEmail() {
        return email;
    }

    public String getIcq() {
        return icq;
    }

    public String getSkype() {
        return skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public Set<Account> getFriends() {
        return friends;
    }

    public Set<Account> getIncomingFriendRequests() {
        return incomingFriendRequests;
    }

    public Set<Account> getOutcomingFriendRequests() {
        return outcomingFriendRequests;
    }

    public List<Group> getCreatedGroups() {
        return createdGroups;
    }

    public Set<Group> getModeratedGroups() {
        return moderatedGroups;
    }

    public Set<Group> getSubscriptions() {
        return subscriptions;
    }

    public Set<Group> getRequestedGroups() {
        return requestedGroups;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public void setLastName(String lastName) {
        requireNonNull(lastName, "Account's last name can't be null");
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        requireNonNull(firstName, "Account's first name can't be null");
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setPersonalPhones(List<Phone> personalPhones) {
        requireNonNull(personalPhones, "Cannot set null as personal phones collection");
        this.personalPhones = personalPhones;
    }

    public void setWorkPhones(List<Phone> workPhones) {
        requireNonNull(workPhones, "Cannot set null as work phones collection");
        this.workPhones = workPhones;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public void setEmail(String email) {
        requireNonNull(email, "Account's email can't be null");
        this.email = email;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void setPassword(String password) {
        requireNonNull(password, "Account's password can't be null");
        this.password = password;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public void setFriends(Set<Account> friends) {
        requireNonNull(friends, "Cannot set null as friends collection");
        this.friends = friends;
    }

    public void setIncomingFriendRequests(Set<Account> incomingFriendRequests) {
        requireNonNull(incomingFriendRequests, "Cannot set null as incoming friendship requests collection");
        this.incomingFriendRequests = incomingFriendRequests;
    }

    public void setOutcomingFriendRequests(Set<Account> outcomingFriendRequests) {
        requireNonNull(outcomingFriendRequests, "Cannot set null as outcoming friendship requests collection");
        this.outcomingFriendRequests = outcomingFriendRequests;
    }

    public void setCreatedGroups(List<Group> createdGroups) {
        requireNonNull(createdGroups, "Cannot set null as created groups collection");
        this.createdGroups = createdGroups;
    }

    public void setModeratedGroups(Set<Group> moderatedGroups) {
        requireNonNull(moderatedGroups, "Cannot set null as moderated groups collection");
        this.moderatedGroups = moderatedGroups;
    }

    public void setSubscriptions(Set<Group> subscriptions) {
        requireNonNull(subscriptions, "Cannot set null as subscriptions to groups collection");
        this.subscriptions = subscriptions;
    }

    public void setRequestedGroups(Set<Group> requestedGroups) {
        requireNonNull(requestedGroups, "Cannot set null as requests to groups collection");
        this.requestedGroups = requestedGroups;
    }

    public void setRoles(Set<Role> roles) {
        requireNonNull(roles, "Cannot set null as account's roles collection");
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Account {id=" + id +
                ", lastName=" + lastName +
                ", firstName=" + firstName +
                ", middleName=" + middleName +
                ", birthDate=" + birthDate +
                ", homeAddress=" + homeAddress +
                ", workAddress=" + workAddress +
                ", email=" + email +
                ", icq=" + icq +
                ", skype=" + skype +
                ", additionalInfo=" + additionalInfo + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }
        Account that = (Account) o;
        return this.email.equalsIgnoreCase(that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(email);
    }
}