package com.getjavajob.training.web1701.dyominn.common.models;

import com.getjavajob.training.web1701.dyominn.common.Entity;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;
import java.util.Objects;

import static java.util.Objects.hash;
import static java.util.Objects.requireNonNull;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;

@javax.persistence.Entity
@Table(name = "phones")
@XStreamAlias("Phone")
public class Phone implements Entity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "phone_id")
    @XStreamOmitField
    private long id;
    @ManyToOne
    @JoinColumn(name = "account_id")
    @XStreamOmitField
    private Account owner;
    private String phoneNumber;
    @Enumerated(STRING)
    private PhoneType type;

    public Phone() {
    }

    public Phone(long id, Account owner, String phoneNumber, PhoneType type) {
        setId(id);
        setOwner(owner);
        setPhoneNumber(phoneNumber);
        setType(type);
    }

    @Override
    public long getId() {
        return id;
    }

    public Account getOwner() {
        return owner;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public PhoneType getType() {
        return type;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public void setOwner(Account owner) {
        requireNonNull(owner, "Phone's owner can't be null");
        this.owner = owner;
    }

    public void setPhoneNumber(String phoneNumber) {
        requireNonNull(phoneNumber, "Phone's number can't be null");
        this.phoneNumber = phoneNumber;
    }

    public void setType(PhoneType type) {
        requireNonNull(type, "Phone's type can't be null");
        this.type = type;
    }

    @Override
    public String toString() {
        return "Phone{id=" + id +
                ", ownerId=" + (owner != null ? owner.getId() : null) +
                ", phoneNumber=" + phoneNumber +
                ", type=" + type + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Phone)) {
            return false;
        }
        Phone that = (Phone) o;
        return Objects.equals(this.owner, that.owner) &&
                Objects.equals(this.phoneNumber, that.phoneNumber) &&
                Objects.equals(this.type, that.type);
    }

    @Override
    public int hashCode() {
        return hash(owner, phoneNumber, type);
    }
}