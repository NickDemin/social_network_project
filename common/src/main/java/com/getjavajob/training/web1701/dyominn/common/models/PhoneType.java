package com.getjavajob.training.web1701.dyominn.common.models;

public enum PhoneType {
    WORK, HOME
}