package com.getjavajob.training.web1701.dyominn.common.models;

import com.getjavajob.training.web1701.dyominn.common.Entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;
import static javax.persistence.GenerationType.IDENTITY;

@javax.persistence.Entity
@Table(name = "groups", uniqueConstraints = @UniqueConstraint(columnNames = "group_name"))
public class Group implements Entity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "group_id")
    private long id;
    @Column(name = "group_name", unique = true, nullable = false)
    private String name;
    private String description;
    private byte[] avatar;
    @ManyToOne
    @JoinColumn(name = "creator_id", nullable = false)
    private Account creator;
    @ManyToMany(mappedBy = "moderatedGroups")
    private Set<Account> moderators = new HashSet<>();
    @ManyToMany(mappedBy = "subscriptions")
    private Set<Account> subscribers = new HashSet<>();
    @ManyToMany(mappedBy = "requestedGroups")
    private Set<Account> requesters = new HashSet<>();

    public Group() {
    }

    public Group(long id, Account creator, String name, String description) {
        setId(id);
        setCreator(creator);
        setName(name);
        setDescription(description);
    }

    @Override
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public Account getCreator() {
        return creator;
    }

    public Set<Account> getModerators() {
        return moderators;
    }

    public Set<Account> getSubscribers() {
        return subscribers;
    }

    public Set<Account> getRequesters() {
        return requesters;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        requireNonNull(name, "Group's name can't be null");
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public void setCreator(Account creator) {
        requireNonNull(creator, "Group's creator can't be null");
        this.creator = creator;
    }

    public void setModerators(Set<Account> moderators) {
        requireNonNull(moderators, "Cannot set group's moderators collection as null");
        this.moderators = moderators;
    }

    public void setSubscribers(Set<Account> subscribers) {
        requireNonNull(subscribers, "Cannot set group's subscribers collection as null");
        this.subscribers = subscribers;
    }

    public void setRequesters(Set<Account> requesters) {
        requireNonNull(requesters, "Cannot set group's requests collection as null");
        this.requesters = requesters;
    }

    @Override
    public String toString() {
        return "Group {id=" + id +
                ", creatorId=" + (creator != null ? creator.getId() : null) +
                ", name=" + name +
                ", description=" + description + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Group)) {
            return false;
        }
        Group that = (Group) o;
        return Objects.equals(this.name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }
}