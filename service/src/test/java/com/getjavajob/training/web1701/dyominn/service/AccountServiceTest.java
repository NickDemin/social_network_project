package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.getjavajob.training.web1701.dyominn.common.models.Role;
import com.getjavajob.training.web1701.dyominn.dao.AccountRepository;
import com.getjavajob.training.web1701.dyominn.dao.RoleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-service-test.xml"})
@Transactional
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Mock
    private AccountRepository accountRepositoryMock;
    @Mock
    private RoleRepository roleRepositoryMock;
    @InjectMocks
    private AccountService accountServiceMock;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Group group1;
    private Group group2;
    private Group group3;

    public AccountServiceTest() {
        init();
    }

    @Before
    public void setupMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getById() {
        when(accountRepositoryMock.findOne(10L)).thenReturn(account2);
        Account actual = accountServiceMock.get(10L);
        assertEquals(account2, actual);
    }

    @Test
    public void getAccountsByNameOrSurnamePattern() {
        List<Account> expected = Arrays.asList(account1, account2);
        when(accountRepositoryMock.findAccountsByStringPattern("query", new PageRequest(0, 2))).thenReturn(expected);
        List<Account> actual = accountServiceMock.getAccountsByNameOrSurnamePattern("query", 0, 2);
        assertEquals(expected, actual);
    }

    @Test
    public void countAccountsWithNameOrSurnamePattern() {
        long expected = 5L;
        when(accountRepositoryMock.countAccountsByStringPattern("query")).thenReturn(expected);
        long actual = accountServiceMock.countAccountsWithNameOrSurnamePattern("query");
        assertEquals(expected, actual);
    }

    @Test
    public void createAccount() {
        when(accountRepositoryMock.save(account1)).thenReturn(account1);
        when(roleRepositoryMock.findByName("ROLE_USER")).thenReturn(new Role(1, "ROLE_USER"));
        Account actual = accountServiceMock.createAccount(account1);
        assertEquals(account1, actual);
    }

    @Test
    public void editAccount() {
        accountServiceMock.editAccount(account1);
        verify(accountRepositoryMock).save(account1);
    }

    @Test
    public void removeFromFriends() {
        assertTrue(account4.getFriends().contains(account3));
        assertTrue(account3.getFriends().contains(account4));
        accountService.removeFromFriends(4, 3, account4);
        Account initiator = accountService.get(4);
        Account removable = accountService.get(3);
        assertFalse(initiator.getFriends().contains(removable));
        assertFalse(removable.getFriends().contains(initiator));
    }

    @Test
    public void removeFromFriendsAndSendToSubscribers() {
        assertTrue(account4.getFriends().contains(account3));
        assertTrue(account3.getFriends().contains(account4));
        assertFalse(account4.getIncomingFriendRequests().contains(account3));
        accountService.removeFromFriends(4, 3, account4);
        Account initiator = accountService.get(4);
        Account removable = accountService.get(3);
        assertTrue(initiator.getIncomingFriendRequests().contains(removable));
    }

    @Test
    public void removeFromFriendsNotFriendAndNotSubscriber() {
        assertFalse(account4.getFriends().contains(account1));
        assertFalse(account1.getFriends().contains(account4));
        assertFalse(account4.getIncomingFriendRequests().contains(account1));
        assertFalse(account1.getOutcomingFriendRequests().contains(account4));
        accountService.removeFromFriends(4, 1, account4);
        Account initiator = accountService.get(4);
        Account notFriend = accountService.get(1);
        assertFalse(initiator.getFriends().contains(notFriend));
        assertFalse(notFriend.getFriends().contains(initiator));
        assertFalse(initiator.getIncomingFriendRequests().contains(notFriend));
        assertFalse(notFriend.getOutcomingFriendRequests().contains(initiator));
    }

    @Test
    public void removeFromFriendsYourself() {
        accountService.removeFromFriends(4, 4, account4);
        Account account = accountService.get(4);
        assertFalse(account.getFriends().contains(account));
        assertFalse(account.getIncomingFriendRequests().contains(account));
        assertFalse(account.getOutcomingFriendRequests().contains(account));
    }

    @Test
    public void removeFromFriendsAndUpdateArgumentInstance() {
        Account removable = accountService.get(3);
        assertTrue(account4.getFriends().contains(removable));
        assertFalse(account4.getIncomingFriendRequests().contains(removable));
        accountService.removeFromFriends(4, 3, account4);
        assertFalse(account4.getFriends().contains(removable));
        assertTrue(account4.getIncomingFriendRequests().contains(removable));
    }

    @Test
    public void removeFromFriendsNoFriendNotUpdateArgumentInstance() {
        Account notFriend = accountService.get(1);
        assertFalse(account4.getFriends().contains(notFriend));
        assertFalse(account4.getIncomingFriendRequests().contains(notFriend));
        accountService.removeFromFriends(4, 1, account4);
        assertFalse(account4.getFriends().contains(notFriend));
        assertFalse(account4.getIncomingFriendRequests().contains(notFriend));
    }

    @Test
    public void acceptFriendshipRequestAndBecomeFriends() {
        assertFalse(account3.getFriends().contains(account1));
        assertFalse(account1.getFriends().contains(account3));
        accountService.acceptFriendshipRequest(3, 1, account3);
        Account receiver = accountService.get(3);
        Account requester = accountService.get(1);
        assertTrue(receiver.getFriends().contains(requester));
        assertTrue(requester.getFriends().contains(receiver));
    }

    @Test
    public void acceptFriendshipRequestAndRemoveIncomingRequest() {
        assertTrue(account3.getIncomingFriendRequests().contains(account1));
        accountService.acceptFriendshipRequest(3, 1, account3);
        Account receiver = accountService.get(3);
        Account requester = accountService.get(1);
        assertFalse(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void acceptFriendshipRequestAndRemoveOutcomingRequest() {
        assertTrue(account1.getOutcomingFriendRequests().contains(account3));
        accountService.acceptFriendshipRequest(3, 1, account3);
        Account receiver = accountService.get(3);
        Account requester = accountService.get(1);
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void acceptFriendshipRequestFromNotRequester() {
        assertFalse(account4.getFriends().contains(account1));
        assertFalse(account1.getFriends().contains(account4));
        assertFalse(account4.getIncomingFriendRequests().contains(account1));
        assertFalse(account1.getOutcomingFriendRequests().contains(account4));
        accountService.acceptFriendshipRequest(4, 1, account4);
        Account initiator = accountService.get(4);
        Account notRequester = accountService.get(1);
        assertFalse(initiator.getFriends().contains(notRequester));
        assertFalse(notRequester.getFriends().contains(initiator));
        assertFalse(initiator.getIncomingFriendRequests().contains(notRequester));
        assertFalse(notRequester.getOutcomingFriendRequests().contains(initiator));
    }

    @Test
    public void acceptFriendshipRequestFromYourself() {
        accountService.acceptFriendshipRequest(4, 4, account4);
        Account account = accountService.get(4);
        assertFalse(account.getFriends().contains(account));
        assertFalse(account.getIncomingFriendRequests().contains(account));
        assertFalse(account.getOutcomingFriendRequests().contains(account));
    }

    @Test
    public void acceptFriendshipRequestAndUpdateArgumentInstance() {
        Account requester = accountService.get(1);
        assertFalse(account3.getFriends().contains(requester));
        assertTrue(account3.getIncomingFriendRequests().contains(requester));
        accountService.acceptFriendshipRequest(3, 1, account3);
        assertTrue(account3.getFriends().contains(requester));
        assertFalse(account3.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void acceptFriendshipRequestFromNotRequesterNotUpdateInstance() {
        Account notRequester = accountService.get(1);
        assertFalse(account4.getFriends().contains(notRequester));
        assertFalse(account4.getIncomingFriendRequests().contains(notRequester));
        accountService.acceptFriendshipRequest(4, 1, account4);
        assertFalse(account4.getFriends().contains(notRequester));
        assertFalse(account4.getIncomingFriendRequests().contains(notRequester));
    }

    @Test
    public void declineFriendshipRequestAndRemoveIncomingRequest() {
        assertTrue(account3.getIncomingFriendRequests().contains(account2));
        accountService.declineFriendshipRequest(3, 2, account3);
        Account receiver = accountService.get(3);
        Account requester = accountService.get(2);
        assertFalse(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void declineFriendshipRequestAndRemoveOutcomingRequest() {
        assertTrue(account2.getOutcomingFriendRequests().contains(account3));
        accountService.declineFriendshipRequest(3, 2, account3);
        Account receiver = accountService.get(3);
        Account requester = accountService.get(2);
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void declineFriendshipRequestAndChangeArgumentInstance() {
        Account requester = accountService.get(2);
        assertTrue(account3.getIncomingFriendRequests().contains(requester));
        accountService.declineFriendshipRequest(3, 2, account3);
        assertFalse(account3.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void cancelOutgoingRequestAndRemoveOutcomingRequest() {
        assertTrue(account1.getOutcomingFriendRequests().contains(account2));
        accountService.cancelOutgoingRequest(1, 2, account1);
        Account requester = accountService.get(1);
        Account receiver = accountService.get(2);
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void cancelOutgoingRequestAndRemoveIncomingRequest() {
        assertTrue(account2.getIncomingFriendRequests().contains(account1));
        accountService.cancelOutgoingRequest(1, 2, account1);
        Account requester = accountService.get(1);
        Account receiver = accountService.get(2);
        assertFalse(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void cancelOutgoingRequestAndChangeArgumentInstance() {
        Account receiver = accountService.get(2);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
        accountService.cancelOutgoingRequest(1, 2, account1);
        assertFalse(account1.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestToOutcomingRequests() {
        assertFalse(account1.getOutcomingFriendRequests().contains(account4));
        accountService.sendFriendshipRequest(1, 4, account1);
        Account requester = accountService.get(1);
        Account receiver = accountService.get(4);
        assertTrue(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestToIncomingRequests() {
        assertFalse(account4.getIncomingFriendRequests().contains(account1));
        accountService.sendFriendshipRequest(1, 4, account1);
        Account requester = accountService.get(1);
        Account receiver = accountService.get(4);
        assertTrue(receiver.getIncomingFriendRequests().contains(requester));
    }

    @Test
    public void sendFriendshipRequestChangeArgumentInstance() {
        Account receiver = accountService.get(4);
        assertFalse(account1.getOutcomingFriendRequests().contains(receiver));
        accountService.sendFriendshipRequest(1, 4, account1);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestToYourself() {
        accountService.sendFriendshipRequest(1, 1, account1);
        Account account = accountService.get(1);
        assertFalse(account.getOutcomingFriendRequests().contains(account));
        assertFalse(account.getIncomingFriendRequests().contains(account));
    }

    @Test
    public void sendFriendshipRequestToYourselfNotChangeArgumentInstance() {
        Account account = accountService.get(1);
        assertFalse(account1.getOutcomingFriendRequests().contains(account));
        accountService.sendFriendshipRequest(1, 1, account1);
        assertFalse(account1.getOutcomingFriendRequests().contains(account));
    }

    @Test
    public void sendFriendshipRequestWhichAlreadyExist() {
        assertTrue(account1.getOutcomingFriendRequests().contains(account2));
        assertTrue(account2.getIncomingFriendRequests().contains(account1));
        accountService.sendFriendshipRequest(1, 2, account1);
        Account requester = accountService.get(1);
        Account receiver = accountService.get(2);
        assertTrue(receiver.getIncomingFriendRequests().contains(requester));
        assertTrue(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestWhichAlreadyExistNotChangeArgumentInstance() {
        Account receiver = accountService.get(2);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
        accountService.sendFriendshipRequest(1, 2, account1);
        assertTrue(account1.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestWithCounterRequestAndRemoveRequests() {
        assertTrue(account2.getIncomingFriendRequests().contains(account1));
        assertTrue(account1.getOutcomingFriendRequests().contains(account2));
        assertFalse(account2.getOutcomingFriendRequests().contains(account1));
        accountService.sendFriendshipRequest(2, 1, account2);
        Account requester = accountService.get(2);
        Account receiver = accountService.get(1);
        assertFalse(requester.getIncomingFriendRequests().contains(receiver));
        assertFalse(receiver.getOutcomingFriendRequests().contains(requester));
        assertFalse(requester.getOutcomingFriendRequests().contains(receiver));
    }

    @Test
    public void sendFriendshipRequestWithCounterRequestMakeFriends() {
        assertFalse(account1.getFriends().contains(account2));
        assertFalse(account2.getFriends().contains(account1));
        accountService.sendFriendshipRequest(2, 1, account2);
        Account requester = accountService.get(2);
        Account receiver = accountService.get(1);
        assertTrue(requester.getFriends().contains(receiver));
        assertTrue(receiver.getFriends().contains(requester));
    }

    @Test
    public void sendFriendshipRequestWithCounterRequestChangeArgumentInstance() {
        Account receiver = accountService.get(1);
        assertTrue(account2.getIncomingFriendRequests().contains(receiver));
        assertFalse(account2.getFriends().contains(receiver));
        accountService.sendFriendshipRequest(2, 1, account2);
        assertFalse(account2.getIncomingFriendRequests().contains(receiver));
        assertTrue(account2.getFriends().contains(receiver));
    }

    @Test
    public void sendGroupRequest() {
        Account account = accountService.createAccount(new Account(0, "User", "user", null, null, null, null, "e@mail",
                null, null, null, "123", null));
        accountService.sendGroupRequest(account, 1);
        Account requester = accountService.get(account.getId());
        Group group = groupService.get(1);
        assertTrue(requester.getRequestedGroups().contains(group));
        assertTrue(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestChangeAccountArgumentInstance() {
        Account account = accountService.createAccount(new Account(0, "User", "user", null, null, null, null, "e@mail",
                null, null, null, "123", null));
        accountService.sendGroupRequest(account, group1.getId());
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void sendGroupRequestByCreator() {
        assertTrue(account4.getCreatedGroups().contains(group1));
        accountService.sendGroupRequest(account4, group1.getId());
        Account requester = accountService.get(4);
        Group group = groupService.get(group1.getId());
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestByCreatorNotChangeAccountArgumentInstance() {
        assertTrue(account4.getCreatedGroups().contains(group1));
        accountService.sendGroupRequest(account4, group1.getId());
        assertFalse(account4.getRequestedGroups().contains(group1));
    }

    @Test
    public void sendGroupRequestByModerator() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        accountService.sendGroupRequest(account3, group2.getId());
        Account requester = accountService.get(3);
        Group group = groupService.get(group2.getId());
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestByModeratorNotChangeAccountArgumentInstance() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        accountService.sendGroupRequest(account3, group2.getId());
        assertFalse(account3.getRequestedGroups().contains(group2));
    }

    @Test
    public void sendGroupRequestBySubscriber() {
        assertTrue(account2.getSubscriptions().contains(group2));
        accountService.sendGroupRequest(account2, group2.getId());
        Account requester = accountService.get(2);
        Group group = groupService.get(group2.getId());
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestBySubscriberNotChangeAccountArgumentInstance() {
        assertTrue(account2.getSubscriptions().contains(group2));
        accountService.sendGroupRequest(account2, group2.getId());
        assertFalse(account2.getRequestedGroups().contains(group2));
    }

    @Test
    public void sendGroupRequestByRequester() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountService.sendGroupRequest(account1, group1.getId());
        Account requester = accountService.get(1);
        Group group = groupService.get(group1.getId());
        assertTrue(requester.getRequestedGroups().contains(group));
        assertTrue(group.getRequesters().contains(requester));
    }

    @Test
    public void sendGroupRequestByRequesterNotChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountService.sendGroupRequest(account1, group1.getId());
        assertTrue(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void leaveGroupByModerator() {
        assertTrue(account3.getModeratedGroups().contains(group3));
        assertTrue(group3.getModerators().contains(account3));
        accountService.leaveGroup(account3, group3.getId());
        Account moderator = accountService.get(3);
        Group group = groupService.get(group3.getId());
        assertFalse(moderator.getModeratedGroups().contains(group));
        assertFalse(moderator.getSubscriptions().contains(group));
        assertFalse(moderator.getRequestedGroups().contains(group));
        assertFalse(group.getModerators().contains(moderator));
        assertFalse(group.getSubscribers().contains(moderator));
        assertFalse(group.getRequesters().contains(moderator));
    }

    @Test
    public void leaveGroupByModeratorChangeAccountArgumentInstance() {
        assertTrue(account3.getModeratedGroups().contains(group3));
        accountService.leaveGroup(account3, group3.getId());
        assertFalse(account3.getModeratedGroups().contains(group3));
        assertFalse(account3.getSubscriptions().contains(group3));
        assertFalse(account3.getRequestedGroups().contains(group3));
    }

    @Test
    public void leaveGroupBySubscriber() {
        assertTrue(account1.getSubscriptions().contains(group3));
        assertTrue(group3.getSubscribers().contains(account1));
        accountService.leaveGroup(account1, group3.getId());
        Account subscriber = accountService.get(1);
        Group group = groupService.get(group3.getId());
        assertFalse(subscriber.getModeratedGroups().contains(group));
        assertFalse(subscriber.getSubscriptions().contains(group));
        assertFalse(subscriber.getRequestedGroups().contains(group));
        assertFalse(group.getModerators().contains(subscriber));
        assertFalse(group.getSubscribers().contains(subscriber));
        assertFalse(group.getRequesters().contains(subscriber));
    }

    @Test
    public void leaveGroupBySubscriberChangeAccountArgumentInstance() {
        assertTrue(account1.getSubscriptions().contains(group3));
        accountService.leaveGroup(account1, group3.getId());
        assertFalse(account1.getModeratedGroups().contains(group3));
        assertFalse(account1.getSubscriptions().contains(group3));
        assertFalse(account1.getRequestedGroups().contains(group3));
    }

    @Test
    public void leaveGroupByRequesterChangeNothing() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertTrue(group1.getRequesters().contains(account1));
        accountService.leaveGroup(account1, group1.getId());
        Account requester = accountService.get(1);
        Group group = groupService.get(1);
        assertFalse(requester.getModeratedGroups().contains(group));
        assertFalse(requester.getSubscriptions().contains(group));
        assertTrue(requester.getRequestedGroups().contains(group));
        assertFalse(group.getModerators().contains(requester));
        assertFalse(group.getSubscribers().contains(requester));
        assertTrue(group.getRequesters().contains(requester));
    }

    @Test
    public void leaveGroupByRequesterNotChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountService.leaveGroup(account1, group1.getId());
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void leaveGroupByNotRelatedAccount() {
        Account account = accountService.createAccount(new Account(0, "User", "user", null, null, null, null, "e@mail",
                null, null, null, "123", null));
        accountService.leaveGroup(account, group1.getId());
        Account managedAccount = accountService.get(account.getId());
        assertFalse(managedAccount.getModeratedGroups().contains(group1));
        assertFalse(managedAccount.getSubscriptions().contains(group1));
        assertFalse(managedAccount.getRequestedGroups().contains(group1));
    }

    @Test
    public void leaveGroupByNotRelatedNotChangeAccountArgumentInstance() {
        Account account = accountService.createAccount(new Account(0, "User", "user", null, null, null, null, "e@mail",
                null, null, null, "123", null));
        accountService.leaveGroup(account, group1.getId());
        assertFalse(account.getModeratedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertFalse(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void cancelRequestToGroupByRequester() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertTrue(group1.getRequesters().contains(account1));
        accountService.cancelRequestToGroup(account1, group1.getId());
        Account requester = accountService.get(1);
        Group group = groupService.get(group1.getId());
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void cancelRequestToGroupByRequesterChangeAccountArgumentInstance() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        accountService.cancelRequestToGroup(account1, group1.getId());
        assertFalse(account1.getRequestedGroups().contains(group1));
    }

    @Test
    public void cancelRequestToGroupWithoutRequest() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertFalse(group1.getRequesters().contains(account3));
        accountService.cancelRequestToGroup(account3, group1.getId());
        Account requester = accountService.get(3);
        Group group = groupService.get(1);
        assertFalse(requester.getRequestedGroups().contains(group));
        assertFalse(group.getRequesters().contains(requester));
    }

    @Test
    public void cancelRequestToGroupWithoutRequestNotChangeArgumentInstance() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        accountService.cancelRequestToGroup(account3, group1.getId());
        assertFalse(account3.getRequestedGroups().contains(group1));
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        group1 = new Group(1, account4, "Empty group", null);
        group2 = new Group(2, account4, "Java", "Java programming language");
        group3 = new Group(3, account4, "Космос", "Космос в картинках и фактах");

        Phone phone1 = new Phone(1, account3, "11111111111", HOME);
        Phone phone2 = new Phone(2, account3, "22222222222", WORK);
        Phone phone3 = new Phone(3, account4, "44444444444", HOME);
        Phone phone4 = new Phone(4, account4, "55555555555", HOME);
        Phone phone5 = new Phone(5, account4, "66666666666", WORK);
        Phone phone6 = new Phone(6, account4, "22222222222", WORK);

        account3.setPersonalPhones(new ArrayList<>(singletonList(phone1)));
        account3.setWorkPhones(new ArrayList<>(singletonList(phone2)));
        account4.setPersonalPhones(new ArrayList<>(asList(phone3, phone4)));
        account4.setWorkPhones(new ArrayList<>(asList(phone5, phone6)));

        account2.setFriends(new HashSet<>(singletonList(account4)));
        account3.setFriends(new HashSet<>(singletonList(account4)));
        account4.setFriends(new HashSet<>(asList(account2, account3)));
        account1.setOutcomingFriendRequests(new HashSet<>(asList(account2, account3)));
        account2.setOutcomingFriendRequests(new HashSet<>(singletonList(account3)));
        account2.setIncomingFriendRequests(new HashSet<>(singletonList(account1)));
        account3.setIncomingFriendRequests(new HashSet<>(asList(account1, account2)));

        account4.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        account2.setModeratedGroups(new HashSet<>(singletonList(group3)));
        account3.setModeratedGroups(new HashSet<>(asList(group2, group3)));
        account1.setSubscriptions(new HashSet<>(asList(group2, group3)));
        account2.setSubscriptions(new HashSet<>(singletonList(group2)));
        account3.setSubscriptions(new HashSet<>(singletonList(group1)));
        account1.setRequestedGroups(new HashSet<>(singletonList(group1)));
        account2.setRequestedGroups(new HashSet<>(singletonList(group1)));

        group1.setModerators(new HashSet<>());
        group1.setSubscribers(new HashSet<>(singletonList(account3)));
        group1.setRequesters(new HashSet<>(asList(account1, account2)));
        group2.setModerators(new HashSet<>(singletonList(account3)));
        group2.setSubscribers(new HashSet<>(asList(account1, account2)));
        group2.setRequesters(new HashSet<>());
        group3.setModerators(new HashSet<>(asList(account2, account3)));
        group3.setSubscribers(new HashSet<>(singletonList(account1)));
        group3.setRequesters(new HashSet<>());
    }
}