package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.getjavajob.training.web1701.dyominn.dao.GroupRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-service-test.xml"})
@Transactional
public class GroupServiceTest {

    @Autowired
    private GroupService groupService;
    @Autowired
    private AccountService accountService;
    @Mock
    private GroupRepository groupRepositoryMock;
    @InjectMocks
    private GroupService groupServiceMock;

    private Account account1;
    private Account account2;
    private Account account3;
    private Group group1;
    private Group group2;
    private Group group3;

    public GroupServiceTest() {
        init();
    }

    @Before
    public void setupMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getById() {
        when(groupRepositoryMock.findOne(10L)).thenReturn(group2);
        Group actual = groupServiceMock.get(10L);
        assertEquals(group2, actual);
    }

    @Test
    public void createGroup() {
        when(groupRepositoryMock.save(group2)).thenReturn(group2);
        Group actual = groupServiceMock.createGroup(group2);
        assertEquals(group2, actual);
    }

    @Test
    public void editGroup() {
        groupServiceMock.editGroup(group2);
        verify(groupRepositoryMock).save(group2);
    }

    @Test
    public void deleteGroup() {
        groupServiceMock.deleteGroup(2L);
        verify(groupRepositoryMock).delete(2L);
    }

    @Test
    public void removeGroupMemberModeratorFromGroupSide() {
        assertTrue(group3.getModerators().contains(account2));
        assertFalse(group3.getSubscribers().contains(account2));
        assertFalse(group3.getRequesters().contains(account2));
        groupService.removeGroupMember(group3, account2.getId());
        Group group = groupService.get(3);
        assertFalse(group.getModerators().contains(account2));
        assertFalse(group.getSubscribers().contains(account2));
        assertFalse(group.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberModeratorFromAccountSide() {
        assertTrue(account2.getModeratedGroups().contains(group3));
        assertFalse(account2.getSubscriptions().contains(group3));
        assertFalse(account2.getRequestedGroups().contains(group3));
        groupService.removeGroupMember(group3, account2.getId());
        Account account = accountService.get(2);
        assertFalse(account.getModeratedGroups().contains(group3));
        assertFalse(account.getSubscriptions().contains(group3));
        assertFalse(account.getRequestedGroups().contains(group3));
    }

    @Test
    public void removeGroupMemberSubscriberFromGroupSide() {
        assertFalse(group2.getModerators().contains(account2));
        assertTrue(group2.getSubscribers().contains(account2));
        assertFalse(group2.getRequesters().contains(account2));
        groupService.removeGroupMember(group2, account2.getId());
        Group group = groupService.get(2);
        assertFalse(group.getModerators().contains(account2));
        assertFalse(group.getSubscribers().contains(account2));
        assertFalse(group.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberSubscriberFromAccountSide() {
        assertFalse(account2.getModeratedGroups().contains(group2));
        assertTrue(account2.getSubscriptions().contains(group2));
        assertFalse(account2.getRequestedGroups().contains(group2));
        groupService.removeGroupMember(group2, account2.getId());
        Account account = accountService.get(2);
        assertFalse(account.getModeratedGroups().contains(group2));
        assertFalse(account.getSubscriptions().contains(group2));
        assertFalse(account.getRequestedGroups().contains(group2));
    }

    @Test
    public void removeGroupMemberRequesterChangeNothingAtGroupSide() {
        assertFalse(group1.getModerators().contains(account2));
        assertFalse(group1.getSubscribers().contains(account2));
        assertTrue(group1.getRequesters().contains(account2));
        groupService.removeGroupMember(group1, account2.getId());
        Group group = groupService.get(1);
        assertFalse(group.getModerators().contains(account2));
        assertFalse(group.getSubscribers().contains(account2));
        assertTrue(group.getRequesters().contains(account2));
    }

    @Test
    public void removeGroupMemberRequesterChangeNothingAtAccountSide() {
        assertFalse(account2.getModeratedGroups().contains(group1));
        assertFalse(account2.getSubscriptions().contains(group1));
        assertTrue(account2.getRequestedGroups().contains(group1));
        groupService.removeGroupMember(group1, account2.getId());
        Account account = accountService.get(2);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void dismissFromModeratorGroupSide() {
        assertTrue(group2.getModerators().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        groupService.dismissFromModerator(group2, account3.getId());
        Group group = groupService.get(2);
        assertFalse(group.getModerators().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
    }

    @Test
    public void dismissFromModeratorAccountSide() {
        assertTrue(account3.getModeratedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        groupService.dismissFromModerator(group2, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getModeratedGroups().contains(group2));
        assertTrue(account.getSubscriptions().contains(group2));
    }

    @Test
    public void dismissFromModeratorWithSubscriberChangeNothingAtGroupSide() {
        assertFalse(group1.getModerators().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        groupService.dismissFromModerator(group1, account3.getId());
        Group group = groupService.get(1);
        assertFalse(group.getModerators().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
    }

    @Test
    public void dismissFromModeratorWithSubscriberChangeNothingAtAccountSide() {
        assertFalse(account3.getModeratedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        groupService.dismissFromModerator(group1, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
    }

    @Test
    public void dismissFromModeratorWithRequesterChangeNothingAtGroupSide() {
        assertFalse(group1.getModerators().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
        groupService.dismissFromModerator(group1, account1.getId());
        Group group = groupService.get(1);
        assertFalse(group.getModerators().contains(account1));
        assertFalse(group.getSubscribers().contains(account1));
        assertTrue(group.getRequesters().contains(account1));
    }

    @Test
    public void dismissFromModeratorWithRequesterChangeNothingAtAccountSide() {
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
        groupService.dismissFromModerator(group1, account1.getId());
        Account account = accountService.get(1);
        assertFalse(account.getModeratedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void promoteToModeratorWithSubscriberGroupSide() {
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupService.promoteToModerator(group1, account3.getId());
        Group group = groupService.get(1);
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void promoteToModeratorWithSubscriberAccountSide() {
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupService.promoteToModerator(group1, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getSubscriptions().contains(group1));
        assertTrue(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void promoteToModeratorWithModeratorChangeNothingAtGroupSide() {
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupService.promoteToModerator(group2, account3.getId());
        Group group = groupService.get(2);
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void promoteToModeratorWithModeratorChangeNothingAtAccountSide() {
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupService.promoteToModerator(group2, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getSubscriptions().contains(group2));
        assertTrue(account.getModeratedGroups().contains(group2));
    }

    @Test
    public void promoteToModeratorWithRequesterChangeNothingAtGroupSide() {
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        assertTrue(group1.getRequesters().contains(account1));
        groupService.promoteToModerator(group1, account1.getId());
        Group group = groupService.get(1);
        assertFalse(group.getSubscribers().contains(account1));
        assertFalse(group.getModerators().contains(account1));
        assertTrue(group.getRequesters().contains(account1));
    }

    @Test
    public void promoteToModeratorWithRequesterChangeNothingAtAccountSide() {
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        assertTrue(account1.getRequestedGroups().contains(group1));
        groupService.promoteToModerator(group1, account1.getId());
        Account account = accountService.get(1);
        assertFalse(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
        assertTrue(account.getRequestedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromRequesterChangeGroupSide() {
        assertTrue(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        groupService.acceptRequest(group1, account1.getId());
        Group group = groupService.get(1);
        assertFalse(group.getRequesters().contains(account1));
        assertTrue(group.getSubscribers().contains(account1));
        assertFalse(group.getModerators().contains(account1));
    }

    @Test
    public void acceptRequestFromRequesterChangeAccountSide() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        groupService.acceptRequest(group1, account1.getId());
        Account account = accountService.get(1);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromSubscriberNotChangeGroupSide() {
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupService.acceptRequest(group1, account3.getId());
        Group group = groupService.get(1);
        assertFalse(group.getRequesters().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
        assertFalse(group.getModerators().contains(account3));
    }

    @Test
    public void acceptRequestFromSubscriberNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupService.acceptRequest(group1, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void acceptRequestFromModeratorNotChangeGroupSide() {
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupService.acceptRequest(group2, account3.getId());
        Group group = groupService.get(2);
        assertFalse(group.getRequesters().contains(account3));
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void acceptRequestFromModeratorNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupService.acceptRequest(group2, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getRequestedGroups().contains(group2));
        assertFalse(account.getSubscriptions().contains(group2));
        assertTrue(account.getModeratedGroups().contains(group2));
    }

    @Test
    public void declineRequestFromRequesterChangeGroupSide() {
        assertTrue(group1.getRequesters().contains(account1));
        assertFalse(group1.getSubscribers().contains(account1));
        assertFalse(group1.getModerators().contains(account1));
        groupService.declineRequest(group1, account1.getId());
        Group group = groupService.get(1);
        assertFalse(group.getRequesters().contains(account1));
        assertFalse(group.getSubscribers().contains(account1));
        assertFalse(group.getModerators().contains(account1));
    }

    @Test
    public void declineRequestFromRequesterChangeAccountSide() {
        assertTrue(account1.getRequestedGroups().contains(group1));
        assertFalse(account1.getSubscriptions().contains(group1));
        assertFalse(account1.getModeratedGroups().contains(group1));
        groupService.declineRequest(group1, account1.getId());
        Account account = accountService.get(1);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertFalse(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void declineRequestFromSubscriberNotChangeGroupSide() {
        assertFalse(group1.getRequesters().contains(account3));
        assertTrue(group1.getSubscribers().contains(account3));
        assertFalse(group1.getModerators().contains(account3));
        groupService.declineRequest(group1, account3.getId());
        Group group = groupService.get(1);
        assertFalse(group.getRequesters().contains(account3));
        assertTrue(group.getSubscribers().contains(account3));
        assertFalse(group.getModerators().contains(account3));
    }

    @Test
    public void declineRequestFromSubscriberNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group1));
        assertTrue(account3.getSubscriptions().contains(group1));
        assertFalse(account3.getModeratedGroups().contains(group1));
        groupService.declineRequest(group1, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getRequestedGroups().contains(group1));
        assertTrue(account.getSubscriptions().contains(group1));
        assertFalse(account.getModeratedGroups().contains(group1));
    }

    @Test
    public void declineRequestFromModeratorNotChangeGroupSide() {
        assertFalse(group2.getRequesters().contains(account3));
        assertFalse(group2.getSubscribers().contains(account3));
        assertTrue(group2.getModerators().contains(account3));
        groupService.declineRequest(group2, account3.getId());
        Group group = groupService.get(2);
        assertFalse(group.getRequesters().contains(account3));
        assertFalse(group.getSubscribers().contains(account3));
        assertTrue(group.getModerators().contains(account3));
    }

    @Test
    public void declineRequestFromModeratorNotChangeAccountSide() {
        assertFalse(account3.getRequestedGroups().contains(group2));
        assertFalse(account3.getSubscriptions().contains(group2));
        assertTrue(account3.getModeratedGroups().contains(group2));
        groupService.declineRequest(group2, account3.getId());
        Account account = accountService.get(3);
        assertFalse(account.getRequestedGroups().contains(group2));
        assertFalse(account.getSubscriptions().contains(group2));
        assertTrue(account.getModeratedGroups().contains(group2));
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        Account account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        group1 = new Group(1, account4, "Empty group", null);
        group2 = new Group(2, account4, "Java", "Java programming language");
        group3 = new Group(3, account4, "Космос", "Космос в картинках и фактах");

        Phone phone1 = new Phone(1, account3, "11111111111", HOME);
        Phone phone2 = new Phone(2, account3, "22222222222", WORK);
        Phone phone3 = new Phone(3, account4, "44444444444", HOME);
        Phone phone4 = new Phone(4, account4, "55555555555", HOME);
        Phone phone5 = new Phone(5, account4, "66666666666", WORK);
        Phone phone6 = new Phone(6, account4, "22222222222", WORK);

        account3.setPersonalPhones(new ArrayList<>(singletonList(phone1)));
        account3.setWorkPhones(new ArrayList<>(singletonList(phone2)));
        account4.setPersonalPhones(new ArrayList<>(asList(phone3, phone4)));
        account4.setWorkPhones(new ArrayList<>(asList(phone5, phone6)));

        account2.setFriends(new HashSet<>(singletonList(account4)));
        account3.setFriends(new HashSet<>(singletonList(account4)));
        account4.setFriends(new HashSet<>(asList(account2, account3)));
        account1.setOutcomingFriendRequests(new HashSet<>(asList(account2, account3)));
        account2.setOutcomingFriendRequests(new HashSet<>(singletonList(account3)));
        account2.setIncomingFriendRequests(new HashSet<>(singletonList(account1)));
        account3.setIncomingFriendRequests(new HashSet<>(asList(account1, account2)));

        account4.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        account2.setModeratedGroups(new HashSet<>(singletonList(group3)));
        account3.setModeratedGroups(new HashSet<>(asList(group2, group3)));
        account1.setSubscriptions(new HashSet<>(asList(group2, group3)));
        account2.setSubscriptions(new HashSet<>(singletonList(group2)));
        account3.setSubscriptions(new HashSet<>(singletonList(group1)));
        account1.setRequestedGroups(new HashSet<>(singletonList(group1)));
        account2.setRequestedGroups(new HashSet<>(singletonList(group1)));

        group1.setModerators(new HashSet<>());
        group1.setSubscribers(new HashSet<>(singletonList(account3)));
        group1.setRequesters(new HashSet<>(asList(account1, account2)));
        group2.setModerators(new HashSet<>(singletonList(account3)));
        group2.setSubscribers(new HashSet<>(asList(account1, account2)));
        group2.setRequesters(new HashSet<>());
        group3.setModerators(new HashSet<>(asList(account2, account3)));
        group3.setSubscribers(new HashSet<>(singletonList(account1)));
        group3.setRequesters(new HashSet<>());
    }
}