INSERT INTO roles VALUES (1, 'ROLE_USER');
INSERT INTO roles VALUES (2, 'ROLE_ADMIN');

INSERT INTO accounts_roles VALUES (1, 1);
INSERT INTO accounts_roles VALUES (1, 2);
INSERT INTO accounts_roles VALUES (2, 1);
INSERT INTO accounts_roles VALUES (3, 1);
INSERT INTO accounts_roles VALUES (4, 1);