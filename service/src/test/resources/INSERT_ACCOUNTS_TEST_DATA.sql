INSERT INTO accounts VALUES
  (DEFAULT, 'Ivanov', 'Ivan', NULL, NULL, NULL, NULL, 'ivanov@gmail.com', NULL, NULL, NULL, '111', NULL);

INSERT INTO accounts VALUES
  (DEFAULT, 'Petrov', 'Petr', 'Petrovich', '2000-01-01', 'Russia, Moscow, Red Square', 'Russia, Moscow, Kremlin',
            'petrov@gmail.com', 'petrov_icq', 'petrov_skype', 'petrov_additional_info', '123', NULL);

INSERT INTO accounts VALUES
  (DEFAULT, 'Petrov', 'Petr', 'Petrovich', '2000-01-01', 'Russia, Moscow, Red Square', 'Russia, Moscow, Kremlin',
            'petrov_copy@gmail.com', 'petrov_icq', 'petrov_skype', 'Copy of Petrov with another e-mail', '123', NULL);

INSERT INTO accounts VALUES
  (DEFAULT, 'Попова', 'Ольга', 'Викторовна', '1990-12-20', 'Россия, г. Омск, ул. Центральная, д. 10-12', 'Домохозяйка',
            'popova@yandex.ru', 'popova_icq', 'popova_skype', 'Пользователь, записанный кириллицей', '12345', NULL);