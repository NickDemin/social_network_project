INSERT INTO groups VALUES (DEFAULT, 'Empty group', NULL, NULL, 4);
INSERT INTO groups VALUES (DEFAULT, 'Java', 'Java programming language', NULL, 4);
INSERT INTO groups VALUES (DEFAULT, 'Космос', 'Космос в картинках и фактах', NULL, 4);

INSERT INTO groups_moderators VALUES (2, 3);
INSERT INTO groups_moderators VALUES (3, 2);
INSERT INTO groups_moderators VALUES (3, 3);

INSERT INTO groups_subscribers VALUES (1, 3);
INSERT INTO groups_subscribers VALUES (2, 1);
INSERT INTO groups_subscribers VALUES (2, 2);
INSERT INTO groups_subscribers VALUES (3, 1);

INSERT INTO groups_requests VALUES (1, 1);
INSERT INTO groups_requests VALUES (1, 2);