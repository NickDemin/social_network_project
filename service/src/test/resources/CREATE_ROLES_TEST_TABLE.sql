CREATE TABLE IF NOT EXISTS roles (
  role_id   BIGINT AUTO_INCREMENT,
  role_name CHAR(20),
  CONSTRAINT role_pk PRIMARY KEY (role_id),
  CONSTRAINT role_uq UNIQUE (role_name)
);

CREATE TABLE IF NOT EXISTS accounts_roles
(
  account_id BIGINT NOT NULL,
  role_id    BIGINT NOT NULL,

  CONSTRAINT accounts_roles_pk PRIMARY KEY (account_id, role_id),
  CONSTRAINT ar_account_fk FOREIGN KEY (account_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT ar_role_fk FOREIGN KEY (role_id) REFERENCES roles (role_id)
    ON DELETE CASCADE
);