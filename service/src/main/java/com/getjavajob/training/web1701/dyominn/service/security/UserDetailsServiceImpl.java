package com.getjavajob.training.web1701.dyominn.service.security;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.dao.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.getjavajob.training.web1701.dyominn.service.utils.ServiceUtils.*;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private AccountRepository accountRepository;

    @Autowired
    public UserDetailsServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.info("Received request for UserDetails by email");
        Account user = accountRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }
        readLazyPhones(user);
        readLazyFriendshipRelations(user);
        readLazyGroups(user);
        readLazyRoles(user);
        logger.info("Return UserDetails instance by email");
        return user;
    }
}