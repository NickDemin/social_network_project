package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Role;
import com.getjavajob.training.web1701.dyominn.dao.AccountRepository;
import com.getjavajob.training.web1701.dyominn.dao.GroupRepository;
import com.getjavajob.training.web1701.dyominn.dao.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.getjavajob.training.web1701.dyominn.service.utils.ServiceUtils.*;

@Service
@Transactional
public class AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    private GroupRepository groupRepository;
    private AccountRepository accountRepository;
    private RoleRepository roleRepository;

    @Autowired
    public AccountService(GroupRepository groupRepository, AccountRepository accountRepository, RoleRepository roleRepository) {
        this.groupRepository = groupRepository;
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
    }

    public Account get(long id) {
        logger.info("Transaction started: get account by id");
        Account account = accountRepository.findOne(id);
        if (account != null) {
            logger.debug("Read all lazy collections");
            readLazyPhones(account);
            readLazyFriendshipRelations(account);
            readLazyGroups(account);
            readLazyRoles(account);
        }
        logger.info("Transaction completed: get account by id");
        return account;
    }

    public List<Account> getAccountsByNameOrSurnamePattern(String pattern, int page, int accountsPerPage) {
        logger.info("Transaction started: get paginated list of accounts with name/surname pattern");
        List<Account> accounts = accountRepository.findAccountsByStringPattern(pattern, new PageRequest(page, accountsPerPage));
        logger.info("Transaction completed: get paginated list of accounts with name/surname pattern");
        return accounts;
    }

    public long countAccountsWithNameOrSurnamePattern(String pattern) {
        logger.info("Transaction started: count accounts with name/surname pattern");
        long accounts = accountRepository.countAccountsByStringPattern(pattern);
        logger.info("Transaction completed: count accounts with name/surname pattern");
        return accounts;
    }

    public Account createAccount(Account account) {
        logger.info("Transaction started: create new account");
        setDefaultUserRole(account);
        Account created = accountRepository.save(account);
        logger.info("Transaction completed: create new account");
        return created;
    }

    public void editAccount(Account account) {
        logger.info("Transaction started: update account");
        accountRepository.save(account);
        logger.info("Transaction completed: update account");
    }

    // Friends relationship logic

    public void removeFromFriends(long initiatorFriendId, long removableFriendId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: remove account from friends");
        if (initiatorFriendId != removableFriendId) {
            Account initiator = accountRepository.findOne(initiatorFriendId);
            Account removable = accountRepository.findOne(removableFriendId);
            boolean wereFriends = initiator.getFriends().remove(removable);
            if (wereFriends) {
                logger.debug("Accounts were friends. Transfer removable friend to followers");
                initiator.getIncomingFriendRequests().add(removable);
                removable.getFriends().remove(initiator);
                removable.getOutcomingFriendRequests().add(initiator);
                updateStateOfFriendshipRelations(initiator, sessionAccountForUpdate);
            }
        }
        logger.info("Transaction completed: remove account from friends");
    }

    public void acceptFriendshipRequest(long receiverId, long requesterId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: accept friendship request");
        if (receiverId != requesterId) {
            Account receiver = accountRepository.findOne(receiverId);
            Account requester = accountRepository.findOne(requesterId);
            boolean haveRequest = receiver.getIncomingFriendRequests().remove(requester);
            if (haveRequest) {
                logger.debug("Friendship request exist. Make accounts friends");
                requester.getOutcomingFriendRequests().remove(receiver);
                receiver.getFriends().add(requester);
                requester.getFriends().add(receiver);
                updateStateOfFriendshipRelations(receiver, sessionAccountForUpdate);
            }
        }
        logger.info("Transaction completed: accept friendship request");
    }

    public void declineFriendshipRequest(long receiverId, long requesterId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: decline friendship request");
        if (receiverId != requesterId) {
            Account receiver = accountRepository.findOne(receiverId);
            Account requester = accountRepository.findOne(requesterId);
            boolean haveIncomingRequest = receiver.getIncomingFriendRequests().remove(requester);
            if (haveIncomingRequest) {
                logger.debug("Incoming friendship request exist. Decline it");
                requester.getOutcomingFriendRequests().remove(receiver);
                updateStateOfFriendshipRelations(receiver, sessionAccountForUpdate);
            }
        }
        logger.info("Transaction completed: decline friendship request");
    }

    public void cancelOutgoingRequest(long requesterId, long receiverId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: cancel outgoing friendship request");
        if (receiverId != requesterId) {
            Account requester = accountRepository.findOne(requesterId);
            Account receiver = accountRepository.findOne(receiverId);
            boolean haveOutcomingRequest = requester.getOutcomingFriendRequests().remove(receiver);
            if (haveOutcomingRequest) {
                logger.debug("Outcoming friendship request exist. Cancel it");
                receiver.getIncomingFriendRequests().remove(requester);
                updateStateOfFriendshipRelations(requester, sessionAccountForUpdate);
            }
        }
        logger.info("Transaction completed: cancel outgoing friendship request");
    }

    public void sendFriendshipRequest(long requesterId, long receiverId, Account sessionAccountForUpdate) {
        logger.info("Transaction started: send outgoing friendship request");
        if (receiverId == requesterId) {
            logger.info("Transaction completed: made an attempt to send friendship request to yourself");
            return;
        }
        Account requester = accountRepository.findOne(requesterId);
        Account receiver = accountRepository.findOne(receiverId);
        if (requester.getOutcomingFriendRequests().contains(receiver)) {
            logger.info("Transaction completed: friendship request already made");
            return;
        }
        boolean haveCounterRequest = requester.getIncomingFriendRequests().contains(receiver);
        if (haveCounterRequest) {
            logger.debug("Counter-request already made. Make accounts as friends");
            requester.getIncomingFriendRequests().remove(receiver);
            receiver.getOutcomingFriendRequests().remove(requester);
            receiver.getFriends().add(requester);
            requester.getFriends().add(receiver);
        } else {
            requester.getOutcomingFriendRequests().add(receiver);
            receiver.getIncomingFriendRequests().add(requester);
        }
        updateStateOfFriendshipRelations(requester, sessionAccountForUpdate);
        logger.info("Transaction completed: send outgoing friendship request");
    }

    // Account's groups logic

    public void sendGroupRequest(Account requester, long groupId) {
        logger.info("Transaction started: send request to group");
        Account account = accountRepository.findOne(requester.getId());
        Group group = groupRepository.findOne(groupId);
        if (account.getRequestedGroups().contains(group)) {
            logger.info("Transaction completed: request to group already exist");
            return;
        }
        if (!isGroupMember(account, group)) {
            logger.debug("Requester is not member of group. Send request");
            account.getRequestedGroups().add(group);
            group.getRequesters().add(account);
            updateStateOfGroups(account, requester);
        }
        logger.info("Transaction completed: send request to group");
    }

    public void leaveGroup(Account member, long groupId) {
        logger.info("Transaction started: leave group");
        Account account = accountRepository.findOne(member.getId());
        Group group = groupRepository.findOne(groupId);
        boolean isSubscriber = account.getSubscriptions().remove(group);
        boolean isModerator = account.getModeratedGroups().remove(group);
        if (isSubscriber || isModerator) {
            logger.debug("Account is subscriber/moderator. Leave the group");
            group.getSubscribers().remove(account);
            group.getModerators().remove(account);
            updateStateOfGroups(account, member);
        }
        logger.info("Transaction completed: leave group");
    }

    public void cancelRequestToGroup(Account requester, long groupId) {
        logger.info("Transaction started: cancel request to group");
        Account account = accountRepository.findOne(requester.getId());
        Group group = groupRepository.findOne(groupId);
        boolean haveRequest = account.getRequestedGroups().remove(group);
        if (haveRequest) {
            logger.debug("Request to group exist. Cancel it");
            group.getRequesters().remove(account);
            updateStateOfGroups(account, requester);
        }
        logger.info("Transaction completed: cancel request to group");
    }

    private boolean isGroupMember(Account account, Group group) {
        return account.getCreatedGroups().contains(group) ||
                account.getModeratedGroups().contains(group) ||
                account.getSubscriptions().contains(group);
    }

    private void setDefaultUserRole(Account account) {
        Role defaultRole = roleRepository.findByName("ROLE_USER");
        Set<Role> roles = new HashSet<>();
        roles.add(defaultRole);
        account.setRoles(roles);
    }

    // Copy state of persistent entities to detached argument instances

    private void updateStateOfFriendshipRelations(Account source, Account destination) {
        logger.info("Started method: copy state of account's friendship relations");
        destination.setFriends(source.getFriends());
        destination.setIncomingFriendRequests(source.getIncomingFriendRequests());
        destination.setOutcomingFriendRequests(source.getOutcomingFriendRequests());
        logger.info("Completed method: copy state of account's friendship relations");
    }

    private void updateStateOfGroups(Account source, Account destination) {
        logger.info("Started method: copy state of account's groups collections");
        destination.setCreatedGroups(source.getCreatedGroups());
        destination.setModeratedGroups(source.getModeratedGroups());
        destination.setSubscriptions(source.getSubscriptions());
        destination.setRequestedGroups(source.getRequestedGroups());
        logger.info("Completed method: copy state of account's groups collections");
    }
}