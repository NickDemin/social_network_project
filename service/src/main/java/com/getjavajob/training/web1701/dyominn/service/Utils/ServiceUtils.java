package com.getjavajob.training.web1701.dyominn.service.utils;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.getjavajob.training.web1701.dyominn.common.models.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

public class ServiceUtils {
    private static final Logger logger = LoggerFactory.getLogger(ServiceUtils.class);


    public static void readLazyPhones(Account account) {
        logger.info("Started method: read lazy phones collections");
        List<Phone> personalPhones = account.getPersonalPhones();
        if (personalPhones != null) {
            personalPhones.size();
        }
        List<Phone> workPhones = account.getWorkPhones();
        if (workPhones != null) {
            workPhones.size();
        }
        logger.info("Completed method: read lazy phones collections");
    }

    public static void readLazyFriendshipRelations(Account account) {
        logger.info("Started method: read lazy friendship relations collections");
        Set<Account> friends = account.getFriends();
        if (friends != null) {
            friends.size();
        }
        Set<Account> incomingRequests = account.getIncomingFriendRequests();
        if (incomingRequests != null) {
            incomingRequests.size();
        }
        Set<Account> outgoingRequests = account.getOutcomingFriendRequests();
        if (outgoingRequests != null) {
            outgoingRequests.size();
        }
        logger.info("Completed method: read lazy friendship relations collections");
    }

    public static void readLazyGroups(Account account) {
        logger.info("Started method: read lazy groups collections");
        List<Group> createdGroups = account.getCreatedGroups();
        if (createdGroups != null) {
            createdGroups.size();
        }
        Set<Group> moderatedGroups = account.getModeratedGroups();
        if (moderatedGroups != null) {
            moderatedGroups.size();
        }
        Set<Group> subscriptions = account.getSubscriptions();
        if (subscriptions != null) {
            subscriptions.size();
        }
        Set<Group> requests = account.getRequestedGroups();
        if (requests != null) {
            requests.size();
        }
        logger.info("Completed method: read lazy groups collections");
    }

    public static void readLazyRoles(Account account) {
        logger.info("Started method: read lazy roles collection");
        Set<Role> roles = account.getRoles();
        if (roles != null) {
            roles.size();
        }
        logger.info("Completed method: read lazy roles collection");
    }
}
