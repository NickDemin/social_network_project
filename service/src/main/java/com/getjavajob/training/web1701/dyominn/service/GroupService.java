package com.getjavajob.training.web1701.dyominn.service;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.dao.AccountRepository;
import com.getjavajob.training.web1701.dyominn.dao.GroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GroupService {
    private static final Logger logger = LoggerFactory.getLogger(GroupService.class);
    private GroupRepository groupRepository;
    private AccountRepository accountRepository;

    @Autowired
    public GroupService(GroupRepository groupRepository, AccountRepository accountRepository) {
        this.groupRepository = groupRepository;
        this.accountRepository = accountRepository;
    }

    public Group get(long id) {
        logger.info("Transaction started: get group by id");
        Group group = groupRepository.findOne(id);
        if (group != null) {
            logger.debug("Loading group's lazy collections");
            group.getRequesters().size();
            group.getSubscribers().size();
            group.getModerators().size();
        }
        logger.info("Transaction completed: get group by id");
        return group;
    }

    public Group createGroup(Group group) {
        logger.info("Transaction started: create new group");
        Group created = groupRepository.save(group);
        logger.info("Transaction completed: create new group");
        return created;
    }

    public void editGroup(Group group) {
        logger.info("Transaction started: update group");
        groupRepository.save(group);
        logger.info("Transaction completed: update group");
    }

    public void deleteGroup(long id) {
        logger.info("Transaction started: delete group");
        groupRepository.delete(id);
        logger.info("Transaction completed: delete group");
    }

    public void removeGroupMember(Group group, long memberId) {
        logger.info("Transaction started: delete group's member");
        Account member = accountRepository.findOne(memberId);
        Group managedGroup = groupRepository.findOne(group.getId());
        boolean isModerator = member.getModeratedGroups().remove(managedGroup);
        boolean isSubscriber = member.getSubscriptions().remove(managedGroup);
        if (isModerator || isSubscriber) {
            managedGroup.getModerators().remove(member);
            managedGroup.getSubscribers().remove(member);
        }
        logger.info("Transaction completed: delete group's member");
    }

    public void dismissFromModerator(Group group, long memberId) {
        logger.info("Transaction started: dismiss moderator");
        Account member = accountRepository.findOne(memberId);
        Group managedGroup = groupRepository.findOne(group.getId());
        boolean isModerator = member.getModeratedGroups().remove(managedGroup);
        if (isModerator) {
            logger.debug("Account is moderator of the group. Transfer to subscribers");
            managedGroup.getModerators().remove(member);
            managedGroup.getSubscribers().add(member);
            member.getSubscriptions().add(managedGroup);
        }
        logger.info("Transaction completed: dismiss moderator");
    }

    public void promoteToModerator(Group group, long memberId) {
        logger.info("Transaction started: promote subscriber to moderator");
        Account member = accountRepository.findOne(memberId);
        Group managedGroup = groupRepository.findOne(group.getId());
        boolean isSubscriber = member.getSubscriptions().remove(managedGroup);
        if (isSubscriber) {
            logger.debug("Account is subscriber of the group. Promote to moderators");
            managedGroup.getSubscribers().remove(member);
            managedGroup.getModerators().add(member);
            member.getModeratedGroups().add(managedGroup);
        }
        logger.info("Transaction completed: promote subscriber to moderator");
    }

    public void acceptRequest(Group group, long requesterId) {
        logger.info("Transaction started: accept request to group");
        Account account = accountRepository.findOne(requesterId);
        Group managedGroup = groupRepository.findOne(group.getId());
        boolean isRequester = account.getRequestedGroups().remove(managedGroup);
        if (isRequester) {
            logger.debug("Account is requester of the group. Transfer to subscribers");
            managedGroup.getRequesters().remove(account);
            managedGroup.getSubscribers().add(account);
            account.getSubscriptions().add(managedGroup);
        }
        logger.info("Transaction completed: accept request to group");
    }

    public void declineRequest(Group group, long requesterId) {
        logger.info("Transaction started: decline request to group");
        Account account = accountRepository.findOne(requesterId);
        Group managedGroup = groupRepository.findOne(group.getId());
        account.getRequestedGroups().remove(managedGroup);
        managedGroup.getRequesters().remove(account);
        logger.info("Transaction completed: decline request to group");
    }
}