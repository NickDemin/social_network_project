package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<Group, Long> {
}