package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByName(String name);
}