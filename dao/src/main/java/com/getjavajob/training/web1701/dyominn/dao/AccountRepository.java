package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {

    Account findByEmail(String email);

    @Query("select a from Account a where " +
            "concat(a.firstName, ' ', a.lastName) like concat(:stringPattern, '%') or " +
            "concat(a.lastName, ' ', a.firstName) like concat(:stringPattern, '%') ")
    List<Account> findAccountsByStringPattern(@Param("stringPattern") String stringPattern, Pageable pageRequest);

    @Query("select count(a) from Account a where " +
            "concat(a.firstName, ' ', a.lastName) like concat(:stringPattern, '%') or " +
            "concat(a.lastName, ' ', a.firstName) like concat(:stringPattern, '%') ")
    long countAccountsByStringPattern(@Param("stringPattern") String stringPattern);
}