package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Role;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-dao-test.xml"})
@Transactional
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AccountRepository accountRepository;

    private Role userRole;
    private Role adminRole;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public RoleRepositoryTest() {
        init();
    }

    @Test
    public void findRoleById() {
        Role actual = roleRepository.findOne(1L);
        assertEquals(userRole, actual);
        assertEquals(userRole.getName(), actual.getName());
    }

    @Test
    public void findGroupByIdWithAccounts() {
        Role actual = roleRepository.findOne(1L);
        assertEquals(userRole.getAccounts(), actual.getAccounts());
    }

    @Test
    public void findRoleByInvalidId() {
        assertNull(roleRepository.findOne(-1L));
        assertNull(roleRepository.findOne(0L));
        assertNull(roleRepository.findOne(1024L));
    }

    @Test
    public void findAll() {
        List<Role> expected = new ArrayList<>();
        expected.add(userRole);
        expected.add(adminRole);
        assertEquals(expected, roleRepository.findAll());
    }

    @Test
    public void insertRoleModel() {
        Role expected = new Role(0, "ROLE_GOD");
        Role inserted = new Role(0, "ROLE_GOD");
        inserted = roleRepository.save(inserted);
        Role actual = roleRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected, actual);
        assertEquals(expected.getName(), actual.getName());
    }

    @Test
    public void insertRoleGenerateId() {
        Role inserted = roleRepository.save(new Role(0, "ROLE_GOD"));
        assertNotEquals(0, inserted.getId());
    }

    @Test
    public void insertRoleWithAccounts() {
        Role expected = new Role(0, "ROLE_GOD");
        expected.setAccounts(new HashSet<>(asList(account2, account3)));
        Role inserted = new Role(0, "ROLE_GOD");
        inserted.setAccounts(new HashSet<>(asList(account2, account3)));
        inserted = roleRepository.save(inserted);
        Role actual = roleRepository.findOne(inserted.getId());
        assertNotSame(expected.getAccounts(), actual.getAccounts());
        assertEquals(expected.getAccounts(), actual.getAccounts());
    }

    @Test
    public void insertGroupWithBusyName() {
        Role role = new Role(0, "ROLE_USER");
        thrown.expect(DataIntegrityViolationException.class);
        roleRepository.save(role);
    }

    @Test
    public void updateGroupModel() {
        userRole.setName("ROLE_SUPER_USER");
        roleRepository.save(userRole);
        Role updatedUserRole = roleRepository.findOne(1L);
        assertNotSame(userRole, updatedUserRole);
        assertEquals(userRole.getName(), updatedUserRole.getName());
    }

    @Test
    public void deleteGroupById() {
        roleRepository.delete(1L);
        List<Role> expected = new ArrayList<>();
        expected.add(adminRole);
        assertEquals(expected, roleRepository.findAll());
    }

    @Test
    public void deleteRoleNotAffectAccounts() {
        assertTrue(userRole.getAccounts().contains(account3));
        roleRepository.delete(1L);
        assertNotNull(accountRepository.findOne(3L));
    }

    @Test
    public void findByName() {
        Role actualUserRole = roleRepository.findByName("ROLE_USER");
        assertNotSame(userRole, actualUserRole);
        assertEquals(userRole, actualUserRole);
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        userRole = new Role(1, "ROLE_USER");
        adminRole = new Role(2, "ROLE_ADMIN");

        userRole.setAccounts(new HashSet<>(asList(account1, account2, account3, account4)));
        adminRole.setAccounts(new HashSet<>(singletonList(account1)));
        account1.getRoles().add(userRole);
        account1.getRoles().add(adminRole);
        account2.getRoles().add(userRole);
        account3.getRoles().add(userRole);
        account4.getRoles().add(userRole);
    }
}