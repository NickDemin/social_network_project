package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-dao-test.xml"})
@Transactional
public class GroupRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private AccountRepository accountRepository;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Group group1;
    private Group group2;
    private Group group3;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public GroupRepositoryTest() {
        init();
    }

    @Test
    public void findGroupById() {
        Group actual3 = groupRepository.findOne(3L);
        assertEquals(group3, actual3);
        assertEquals(group3.getName(), actual3.getName());
        assertEquals(group3.getDescription(), actual3.getDescription());
        assertEquals(group3.getCreator(), actual3.getCreator());
        assertArrayEquals(group3.getAvatar(), actual3.getAvatar());
    }

    @Test
    public void findGroupByIdWithModerators() {
        Group actual3 = groupRepository.findOne(3L);
        assertEquals(group3.getModerators(), actual3.getModerators());
    }

    @Test
    public void findGroupByIdWithSubscribers() {
        Group actual3 = groupRepository.findOne(3L);
        assertEquals(group3.getSubscribers(), actual3.getSubscribers());
    }

    @Test
    public void findGroupByIdWithRequesters() {
        Group actual3 = groupRepository.findOne(3L);
        assertEquals(group3.getRequesters(), actual3.getRequesters());
    }

    @Test
    public void findGroupByInvalidId() {
        assertNull(groupRepository.findOne(-1L));
        assertNull(groupRepository.findOne(0L));
        assertNull(groupRepository.findOne(1024L));
    }

    @Test
    public void findAll() {
        List<Group> expected = new ArrayList<>();
        expected.add(group1);
        expected.add(group2);
        expected.add(group3);
        assertEquals(expected, groupRepository.findAll());
    }

    @Test
    public void insertGroupModel() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setAvatar(new byte[]{4, 8, 15, 16, 23, 42});
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setAvatar(new byte[]{4, 8, 15, 16, 23, 42});
        inserted = groupRepository.save(inserted);
        Group actual = groupRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected, actual);
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreator(), actual.getCreator());
        assertArrayEquals(expected.getAvatar(), actual.getAvatar());
    }

    @Test
    public void insertGroupGenerateId() {
        Group inserted = groupRepository.save(new Group(0, account1, "testGroup", "testDescription"));
        assertNotEquals(0, inserted.getId());
    }

    @Test
    public void insertGroupWithModerators() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setModerators(new HashSet<>(asList(account2, account3)));
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setModerators(new HashSet<>(asList(account2, account3)));
        inserted = groupRepository.save(inserted);
        Group actual = groupRepository.findOne(inserted.getId());
        assertNotSame(expected.getModerators(), actual.getModerators());
        assertEquals(expected.getModerators(), actual.getModerators());
    }

    @Test
    public void insertGroupWithSubscribers() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setSubscribers(new HashSet<>(asList(account3, account4)));
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setSubscribers(new HashSet<>(asList(account3, account4)));
        inserted = groupRepository.save(inserted);
        Group actual = groupRepository.findOne(inserted.getId());
        assertNotSame(expected.getSubscribers(), actual.getSubscribers());
        assertEquals(expected.getSubscribers(), actual.getSubscribers());
    }

    @Test
    public void insertGroupWithRequesters() {
        Group expected = new Group(0, account1, "testGroup", "testDescription");
        expected.setRequesters(new HashSet<>(asList(account2, account4)));
        Group inserted = new Group(0, account1, "testGroup", "testDescription");
        inserted.setRequesters(new HashSet<>(asList(account2, account4)));
        inserted = groupRepository.save(inserted);
        Group actual = groupRepository.findOne(inserted.getId());
        assertNotSame(expected.getRequesters(), actual.getRequesters());
        assertEquals(expected.getRequesters(), actual.getRequesters());
    }

    @Test
    public void insertGroupWithBusyName() {
        Group group = new Group(0, account4, "Java", "Java programming language");
        thrown.expect(DataIntegrityViolationException.class);
        groupRepository.save(group);
    }

    @Test
    public void updateGroupModel() {
        group1.setCreator(account1);
        group1.setName("Updated name");
        group1.setDescription("Updated description");
        group1.setAvatar(new byte[]{1, 2, 3, 4, 5});
        groupRepository.save(group1);
        Group updatedGroup1 = groupRepository.findOne(1L);
        assertNotSame(group1, updatedGroup1);
        assertEquals(group1.getCreator(), updatedGroup1.getCreator());
        assertEquals(group1.getName(), updatedGroup1.getName());
        assertEquals(group1.getDescription(), updatedGroup1.getDescription());
        assertArrayEquals(group1.getAvatar(), updatedGroup1.getAvatar());
    }

    @Test
    public void deleteGroupById() {
        groupRepository.delete(1L);
        groupRepository.delete(3L);
        List<Group> expected = new ArrayList<>();
        expected.add(group2);
        assertEquals(expected, groupRepository.findAll());
    }

    @Test
    public void deleteGroupNotAffectCreatorAccount() {
        assertTrue(group1.getCreator().equals(account4));
        groupRepository.delete(1L);
        assertNotNull(accountRepository.findOne(4L));
    }

    @Test
    public void deleteGroupNotAffectModeratorsAccounts() {
        assertTrue(group2.getModerators().contains(account3));
        groupRepository.delete(2L);
        assertNotNull(accountRepository.findOne(3L));
    }

    @Test
    public void deleteGroupNotAffectSubscribersAccounts() {
        assertTrue(group1.getSubscribers().contains(account3));
        groupRepository.delete(1L);
        assertNotNull(accountRepository.findOne(3L));
    }

    @Test
    public void deleteGroupNotAffectRequestersAccounts() {
        assertTrue(group1.getRequesters().contains(account1));
        assertTrue(group1.getRequesters().contains(account2));
        groupRepository.delete(1L);
        assertNotNull(accountRepository.findOne(1L));
        assertNotNull(accountRepository.findOne(2L));
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        group1 = new Group(1, account4, "Empty group", null);
        group2 = new Group(2, account4, "Java", "Java programming language");
        group3 = new Group(3, account4, "Космос", "Космос в картинках и фактах");

        Phone phone1 = new Phone(1, account3, "11111111111", HOME);
        Phone phone2 = new Phone(2, account3, "22222222222", WORK);
        Phone phone3 = new Phone(3, account4, "44444444444", HOME);
        Phone phone4 = new Phone(4, account4, "55555555555", HOME);
        Phone phone5 = new Phone(5, account4, "66666666666", WORK);
        Phone phone6 = new Phone(6, account4, "22222222222", WORK);

        account3.setPersonalPhones(new ArrayList<>(singletonList(phone1)));
        account3.setWorkPhones(new ArrayList<>(singletonList(phone2)));
        account4.setPersonalPhones(new ArrayList<>(asList(phone3, phone4)));
        account4.setWorkPhones(new ArrayList<>(asList(phone5, phone6)));

        account2.setFriends(new HashSet<>(singletonList(account4)));
        account3.setFriends(new HashSet<>(singletonList(account4)));
        account4.setFriends(new HashSet<>(asList(account2, account3)));
        account1.setOutcomingFriendRequests(new HashSet<>(asList(account2, account3)));
        account2.setOutcomingFriendRequests(new HashSet<>(singletonList(account3)));
        account2.setIncomingFriendRequests(new HashSet<>(singletonList(account1)));
        account3.setIncomingFriendRequests(new HashSet<>(asList(account1, account2)));

        account4.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        account2.setModeratedGroups(new HashSet<>(singletonList(group3)));
        account3.setModeratedGroups(new HashSet<>(asList(group2, group3)));
        account1.setSubscriptions(new HashSet<>(asList(group2, group3)));
        account2.setSubscriptions(new HashSet<>(singletonList(group2)));
        account3.setSubscriptions(new HashSet<>(singletonList(group1)));
        account1.setRequestedGroups(new HashSet<>(singletonList(group1)));
        account2.setRequestedGroups(new HashSet<>(singletonList(group1)));

        group1.setModerators(new HashSet<>());
        group1.setSubscribers(new HashSet<>(singletonList(account3)));
        group1.setRequesters(new HashSet<>(asList(account1, account2)));
        group2.setModerators(new HashSet<>(singletonList(account3)));
        group2.setSubscribers(new HashSet<>(asList(account1, account2)));
        group2.setRequesters(new HashSet<>());
        group3.setModerators(new HashSet<>(asList(account2, account3)));
        group3.setSubscribers(new HashSet<>(singletonList(account1)));
        group3.setRequesters(new HashSet<>());
    }
}