package com.getjavajob.training.web1701.dyominn.dao;

import com.getjavajob.training.web1701.dyominn.common.models.Account;
import com.getjavajob.training.web1701.dyominn.common.models.Group;
import com.getjavajob.training.web1701.dyominn.common.models.Phone;
import com.getjavajob.training.web1701.dyominn.common.models.Role;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.HOME;
import static com.getjavajob.training.web1701.dyominn.common.models.PhoneType.WORK;
import static java.time.LocalDate.parse;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-dao-test.xml"})
@Transactional
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private GroupRepository groupRepository;

    private Account account1;
    private Account account2;
    private Account account3;
    private Account account4;
    private Group group1;
    private Group group2;
    private Group group3;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    public AccountRepositoryTest() {
        init();
    }

    @Test
    public void findAccountById() {
        Account actual2 = accountRepository.findOne(2L);
        assertEquals(account2, actual2);
        assertEquals(account2.getLastName(), actual2.getLastName());
        assertEquals(account2.getFirstName(), actual2.getFirstName());
        assertEquals(account2.getMiddleName(), actual2.getMiddleName());
        assertEquals(account2.getBirthDate(), actual2.getBirthDate());
        assertEquals(account2.getHomeAddress(), actual2.getHomeAddress());
        assertEquals(account2.getWorkAddress(), actual2.getWorkAddress());
        assertEquals(account2.getIcq(), actual2.getIcq());
        assertEquals(account2.getSkype(), actual2.getSkype());
        assertEquals(account2.getAdditionalInfo(), actual2.getAdditionalInfo());
        assertArrayEquals(account2.getAvatar(), actual2.getAvatar());
    }

    @Test
    public void findAccountByIdWithPhones() {
        Account actual4 = accountRepository.findOne(4L);
        assertEquals(account4.getWorkPhones(), actual4.getWorkPhones());
        assertEquals(account4.getPersonalPhones(), actual4.getPersonalPhones());
    }

    @Test
    public void findAccountByIdWithFriends() {
        Account actual4 = accountRepository.findOne(4L);
        assertEquals(account4.getFriends(), actual4.getFriends());
    }

    @Test
    public void findAccountByIdWithIncomingFriendshipRequests() {
        Account actual3 = accountRepository.findOne(3L);
        assertEquals(account3.getIncomingFriendRequests(), actual3.getIncomingFriendRequests());
    }

    @Test
    public void findAccountByIdWithOutcomingFriendshipRequests() {
        Account actual1 = accountRepository.findOne(1L);
        assertEquals(account1.getOutcomingFriendRequests(), actual1.getOutcomingFriendRequests());
    }

    @Test
    public void findAccountByIdWithCreatedGroups() {
        Account actual4 = accountRepository.findOne(4L);
        assertEquals(account4.getCreatedGroups(), actual4.getCreatedGroups());
    }

    @Test
    public void findAccountByIdWithModeratedGroups() {
        Account actual3 = accountRepository.findOne(3L);
        assertEquals(account3.getModeratedGroups(), actual3.getModeratedGroups());
    }

    @Test
    public void findAccountByIdWithGroupSubscriptions() {
        Account actual1 = accountRepository.findOne(1L);
        assertEquals(account1.getSubscriptions(), actual1.getSubscriptions());
    }

    @Test
    public void findAccountByIdWithGroupRequests() {
        Account actual1 = accountRepository.findOne(1L);
        assertEquals(account1.getRequestedGroups(), actual1.getRequestedGroups());
    }

    @Test
    public void findAccountByInvalidId() {
        assertNull(accountRepository.findOne(-1L));
        assertNull(accountRepository.findOne(0L));
        assertNull(accountRepository.findOne(1024L));
    }

    @Test
    public void insertAccountModel() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", "Middle name", parse("2000-01-01"), "Home", "Work",
                "user@mail.com", "icq", "skype", "additional info", "123", new byte[]{4, 8, 15, 16, 23, 42});
        Account inserted = accountRepository.save(new Account(0, "Last name", "First name", "Middle name", parse("2000-01-01"),
                "Home", "Work", "user@mail.com", "icq", "skype", "additional info", "123", new byte[]{4, 8, 15, 16, 23, 42}));
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected, actual);
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getMiddleName(), actual.getMiddleName());
        assertEquals(expected.getBirthDate(), actual.getBirthDate());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());
        assertEquals(expected.getWorkAddress(), actual.getWorkAddress());
        assertEquals(expected.getIcq(), actual.getIcq());
        assertEquals(expected.getSkype(), actual.getSkype());
        assertEquals(expected.getAdditionalInfo(), actual.getAdditionalInfo());
        assertArrayEquals(expected.getAvatar(), actual.getAvatar());
    }

    @Test
    public void insertAccountGenerateId() {
        Account inserted = accountRepository.save(new Account(0, "Last name", "First name", "Middle name", parse("2000-01-01"),
                "Home", "Work", "user@mail.com", "icq", "skype", "additional info", "123", new byte[]{4, 8, 15, 16, 23, 42}));
        assertNotEquals(0, inserted.getId());
    }

    @Test
    public void insertAccountWithPhones() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.getWorkPhones().add(new Phone(0, expected, "789", WORK));
        expected.getWorkPhones().add(new Phone(0, expected, "012", WORK));
        expected.getPersonalPhones().add(new Phone(0, expected, "123", HOME));
        expected.getPersonalPhones().add(new Phone(0, expected, "456", HOME));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.getWorkPhones().add(new Phone(0, inserted, "789", WORK));
        inserted.getWorkPhones().add(new Phone(0, inserted, "012", WORK));
        inserted.getPersonalPhones().add(new Phone(0, inserted, "123", HOME));
        inserted.getPersonalPhones().add(new Phone(0, inserted, "456", HOME));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getPersonalPhones(), actual.getPersonalPhones());
        assertEquals(expected.getWorkPhones(), actual.getWorkPhones());
    }

    @Test
    public void insertAccountWithFriends() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setFriends(new HashSet<>(asList(account1, account2, account3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setFriends(new HashSet<>(asList(account1, account2, account3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getFriends(), actual.getFriends());
    }

    @Test
    public void insertAccountWithIncomingFriendRequests() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setIncomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setIncomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getIncomingFriendRequests(), actual.getIncomingFriendRequests());
    }

    @Test
    public void insertAccountWithOutcomingFriendRequests() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setOutcomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setOutcomingFriendRequests(new HashSet<>(asList(account1, account2, account3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getOutcomingFriendRequests(), actual.getOutcomingFriendRequests());
    }

    @Test
    public void insertAccountWithCreatedGroups() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getCreatedGroups(), actual.getCreatedGroups());
    }

    @Test
    public void insertAccountWithModeratedGroups() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setModeratedGroups(new HashSet<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setModeratedGroups(new HashSet<>(asList(group1, group2, group3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getModeratedGroups(), actual.getModeratedGroups());
    }

    @Test
    public void insertAccountWithGroupsSubscriptions() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setSubscriptions(new HashSet<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setSubscriptions(new HashSet<>(asList(group1, group2, group3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getSubscriptions(), actual.getSubscriptions());
    }

    @Test
    public void insertAccountWithGroupsRequests() {
        // duplicate account to avoid using the same instance in tests
        Account expected = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        expected.setRequestedGroups(new HashSet<>(asList(group1, group2, group3)));
        Account inserted = new Account(0, "Last name", "First name", null, null, null, null, "user@mail.com", null,
                null, null, "111", null);
        inserted.setRequestedGroups(new HashSet<>(asList(group1, group2, group3)));
        inserted = accountRepository.save(inserted);
        Account actual = accountRepository.findOne(inserted.getId());
        assertNotSame(expected, actual);
        assertEquals(expected.getRequestedGroups(), actual.getRequestedGroups());
    }

    @Test
    public void insertAccountWithBusyEmail() {
        Account account = new Account(0, "Last name", "First name", null, null, null, null, "petrov@gmail.com", null,
                null, null, "111", null);
        thrown.expect(DataIntegrityViolationException.class);
        accountRepository.save(account);
    }

    @Test
    public void updateAccountModel() {
        account1.setLastName("Обновленный");
        account1.setFirstName("Пользователь");
        account1.setMiddleName("Новая фамилия");
        account1.setBirthDate(parse("2050-07-03"));
        account1.setHomeAddress("Новый домашний адрес");
        account1.setWorkAddress("Новый рабочий адрес");
        account1.setEmail("newPost@post.org");
        account1.setIcq("newIcq");
        account1.setSkype("newSkype");
        account1.setAdditionalInfo("Обновленный 1 пользователь");
        account1.setPassword("Новый password");
        account1.setAvatar(new byte[]{4, 8, 15, 16, 23, 42});
        accountRepository.save(account1);
        Account updatedAccount1 = accountRepository.findOne(1L);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1, updatedAccount1);
        assertEquals(account1.getLastName(), updatedAccount1.getLastName());
        assertEquals(account1.getFirstName(), updatedAccount1.getFirstName());
        assertEquals(account1.getMiddleName(), updatedAccount1.getMiddleName());
        assertEquals(account1.getBirthDate(), updatedAccount1.getBirthDate());
        assertEquals(account1.getHomeAddress(), updatedAccount1.getHomeAddress());
        assertEquals(account1.getWorkAddress(), updatedAccount1.getWorkAddress());
        assertEquals(account1.getEmail(), updatedAccount1.getEmail());
        assertEquals(account1.getIcq(), updatedAccount1.getIcq());
        assertEquals(account1.getSkype(), updatedAccount1.getSkype());
        assertEquals(account1.getAdditionalInfo(), updatedAccount1.getAdditionalInfo());
        assertEquals(account1.getPassword(), updatedAccount1.getPassword());
        assertArrayEquals(account1.getAvatar(), updatedAccount1.getAvatar());
    }

    @Test
    public void updateAccountWithWorkPhones() {
        account4.getWorkPhones().clear();
        account4.getWorkPhones().add(new Phone(0, account4, "111", WORK));
        account4.getWorkPhones().add(new Phone(0, account4, "222", WORK));
        accountRepository.save(account4);
        Account updatedAccount4 = accountRepository.findOne(4L);
        assertNotSame(account4, updatedAccount4);
        assertEquals(account4.getWorkPhones(), updatedAccount4.getWorkPhones());
    }

    @Test
    public void updateAccountWithPersonalPhones() {
        account4.getPersonalPhones().clear();
        account4.getPersonalPhones().add(new Phone(0, account4, "333", HOME));
        account4.getPersonalPhones().add(new Phone(0, account4, "444", HOME));
        accountRepository.save(account4);
        Account updatedAccount4 = accountRepository.findOne(4L);
        assertNotSame(account4, updatedAccount4);
        assertEquals(account4.getPersonalPhones(), updatedAccount4.getPersonalPhones());
    }

    @Test
    public void updateAccountWithFriends() {
        account4.getFriends().clear();
        account4.getFriends().add(account1);
        accountRepository.save(account4);
        Account updatedAccount4 = accountRepository.findOne(4L);
        assertNotSame(account4, updatedAccount4);
        assertEquals(account4.getFriends(), updatedAccount4.getFriends());
    }

    @Test
    public void updateAccountWithIncomingFriendRequests() {
        account3.getIncomingFriendRequests().clear();
        account3.getIncomingFriendRequests().add(account4);
        accountRepository.save(account3);
        Account updatedAccount3 = accountRepository.findOne(3L);
        assertNotSame(account3, updatedAccount3);
        assertEquals(account3.getIncomingFriendRequests(), updatedAccount3.getIncomingFriendRequests());
    }

    @Test
    public void updateAccountWithCreatedGroups() {
        account1.getCreatedGroups().clear();
        account1.getCreatedGroups().add(new Group(0, account1, "Test group", "Test description"));
        account1.getCreatedGroups().add(new Group(0, account1, "Test group 2", "Test description 2"));
        accountRepository.save(account1);
        Account updatedAccount1 = accountRepository.findOne(1L);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1.getCreatedGroups(), updatedAccount1.getCreatedGroups());
    }

    @Test
    public void updateAccountWithModeratedGroups() {
        account3.getModeratedGroups().clear();
        account3.getModeratedGroups().add(new Group(0, account3, "Test group", "Test description"));
        account3.getModeratedGroups().add(new Group(0, account3, "Test group 2", "Test description 2"));
        accountRepository.save(account3);
        Account updatedAccount3 = accountRepository.findOne(3L);
        assertNotSame(account3, updatedAccount3);
        assertEquals(account3.getModeratedGroups(), updatedAccount3.getModeratedGroups());
    }

    @Test
    public void updateAccountWithGroupSubscriptions() {
        account1.getSubscriptions().clear();
        account1.getSubscriptions().add(new Group(0, account1, "Test group", "Test description"));
        account1.getSubscriptions().add(new Group(0, account1, "Test group 2", "Test description 2"));
        accountRepository.save(account1);
        Account updatedAccount1 = accountRepository.findOne(1L);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1.getSubscriptions(), updatedAccount1.getSubscriptions());
    }

    @Test
    public void updateAccountWithGroupRequests() {
        account1.getRequestedGroups().clear();
        account1.getRequestedGroups().add(new Group(0, account1, "Test group", "Test description"));
        account1.getRequestedGroups().add(new Group(0, account1, "Test group 2", "Test description 2"));
        accountRepository.save(account1);
        Account updatedAccount1 = accountRepository.findOne(1L);
        assertNotSame(account1, updatedAccount1);
        assertEquals(account1.getRequestedGroups(), updatedAccount1.getRequestedGroups());
    }

    @Test
    public void deleteAccountById() {
        accountRepository.delete(1L);
        accountRepository.delete(4L);
        List<Account> expected = new ArrayList<>();
        expected.add(account2);
        expected.add(account3);
        assertEquals(expected, accountRepository.findAll());
    }

    @Test
    public void deleteAccountByIdWithCreatedGroups() {
        List<Group> beforeDelete = new ArrayList<>(asList(group1, group2, group3));
        assertEquals(beforeDelete, groupRepository.findAll());
        accountRepository.delete(4L);
        List<Group> expected = new ArrayList<>();
        assertEquals(expected, groupRepository.findAll());
    }

    @Test
    public void findAccountByEmail() {
        assertEquals(account2, accountRepository.findByEmail("petrov@gmail.com"));
    }

    @Test
    public void findAccountByNotExistingEmail() {
        assertNull(accountRepository.findByEmail("no such email"));
    }

    @Test
    public void findAccountsByStringPatternAll() {
        List<Account> expected = new ArrayList<>();
        String pattern = "Pattern";
        for (int i = 0; i < 3; i++) {
            String lastName = pattern + i;
            String email = "email_first_case" + i;
            Account account = new Account(0, lastName, "firstName", null, null, null, null, email, null, null, null, "123", null);
            accountRepository.save(account);
            expected.add(account);
        }
        for (int i = 0; i < 3; i++) {
            String firstName = pattern + i;
            String email = "email_second_case" + i;
            Account account = new Account(0, "lastName", firstName, null, null, null, null, email, null, null, null, "123", null);
            accountRepository.save(account);
            expected.add(account);
        }
        List<Account> actual = accountRepository.findAccountsByStringPattern(pattern, new PageRequest(0, 10));
        assertEquals(expected, actual);
    }

    @Test
    public void findAccountsByStringPatternPage() {
        List<Account> expectedFirstPage = new ArrayList<>();
        List<Account> expectedSecondPage = new ArrayList<>();
        String pattern = "Pattern";
        for (int i = 0; i < 3; i++) {
            String lastName = pattern + i;
            String email = "email_first_case" + i;
            Account account = new Account(0, lastName, "firstName", null, null, null, null, email, null, null, null, "123", null);
            accountRepository.save(account);
            expectedFirstPage.add(account);
        }
        for (int i = 0; i < 3; i++) {
            String firstName = pattern + i;
            String email = "email_second_case" + i;
            Account account = new Account(0, "lastName", firstName, null, null, null, null, email, null, null, null, "123", null);
            accountRepository.save(account);
            expectedSecondPage.add(account);
        }
        List<Account> actualFirstPage = accountRepository.findAccountsByStringPattern(pattern, new PageRequest(0, 3));
        List<Account> actualSecondPage = accountRepository.findAccountsByStringPattern(pattern, new PageRequest(1, 3));
        assertEquals(expectedFirstPage, actualFirstPage);
        assertEquals(expectedSecondPage, actualSecondPage);
    }

    @Test
    public void findAccountsByCyrillicStringPattern() {
        List<Account> expected = new ArrayList<>(singletonList(account4));
        List<Account> actual = accountRepository.findAccountsByStringPattern("Попов", new PageRequest(0, 10));
        assertEquals(expected, actual);
    }

    @Test
    public void findAccountsByEmptyStringPattern() {
        List<Account> expected = new ArrayList<>(asList(account1, account2, account3, account4));
        List<Account> actual = accountRepository.findAccountsByStringPattern("", new PageRequest(0, 10));
        assertEquals(expected, actual);
    }

    @Test
    public void findAccountsByNotExistingStringPattern() {
        List<Account> expected = new ArrayList<>();
        List<Account> actual = accountRepository.findAccountsByStringPattern("No such pattern", new PageRequest(1, 5));
        assertEquals(expected, actual);
    }

    @Test
    public void countAccountsByStringPattern() {
        String pattern = "Pattern";
        for (int i = 0; i < 3; i++) {
            String lastName = pattern + i;
            String email = "email_first_case" + i;
            Account account = new Account(0, lastName, "firstName", null, null, null, null, email, null, null, null, "123", null);
            accountRepository.save(account);
        }
        for (int i = 0; i < 3; i++) {
            String firstName = pattern + i;
            String email = "email_second_case" + i;
            Account account = new Account(0, "lastName", firstName, null, null, null, null, email, null, null, null, "123", null);
            accountRepository.save(account);
        }
        assertEquals(6, accountRepository.countAccountsByStringPattern(pattern));
    }

    @Test
    public void countAccountsWithNotExistingNameOrSurnamePattern() {
        assertEquals(0, accountRepository.countAccountsByStringPattern("No such pattern"));
    }

    @Test
    public void accountRolesTest() {
        Role user = new Role(1, "ROLE_USER");
        Role admin = new Role(2, "ROLE_ADMIN");
        Set<Role> expected = new HashSet<>(Arrays.asList(user, admin));
        Account account = accountRepository.findOne(1L);
        assertEquals(expected, account.getRoles());
    }

    private void init() {
        account1 = new Account(1, "Ivanov", "Ivan", null, null, null, null, "ivanov@gmail.com", null, null, null, "111", null);
        account2 = new Account(2, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov@gmail.com", "petrov_icq", "petrov_skype",
                "petrov_additional_info", "123", null);
        account3 = new Account(3, "Petrov", "Petr", "Petrovich", parse("2000-01-01"),
                "Russia, Moscow, Red Square", "Russia, Moscow, Kremlin", "petrov_copy@gmail.com", "petrov_icq", "petrov_skype",
                "Copy of Petrov with another e-mail", "123", null);
        account4 = new Account(4, "Попова", "Ольга", "Викторовна", parse("1990-12-20"),
                "Россия, г. Омск, ул. Центральная, д. 10-12", "Домохозяйка", "popova@yandex.ru", "popova_icq",
                "popova_skype", "Пользователь, записанный кириллицей", "12345", null);

        group1 = new Group(1, account4, "Empty group", null);
        group2 = new Group(2, account4, "Java", "Java programming language");
        group3 = new Group(3, account4, "Космос", "Космос в картинках и фактах");

        Phone phone1 = new Phone(1, account3, "11111111111", HOME);
        Phone phone2 = new Phone(2, account3, "22222222222", WORK);
        Phone phone3 = new Phone(3, account4, "44444444444", HOME);
        Phone phone4 = new Phone(4, account4, "55555555555", HOME);
        Phone phone5 = new Phone(5, account4, "66666666666", WORK);
        Phone phone6 = new Phone(6, account4, "22222222222", WORK);

        account3.setPersonalPhones(new ArrayList<>(singletonList(phone1)));
        account3.setWorkPhones(new ArrayList<>(singletonList(phone2)));
        account4.setPersonalPhones(new ArrayList<>(asList(phone3, phone4)));
        account4.setWorkPhones(new ArrayList<>(asList(phone5, phone6)));

        account2.setFriends(new HashSet<>(singletonList(account4)));
        account3.setFriends(new HashSet<>(singletonList(account4)));
        account4.setFriends(new HashSet<>(asList(account2, account3)));
        account1.setOutcomingFriendRequests(new HashSet<>(asList(account2, account3)));
        account2.setOutcomingFriendRequests(new HashSet<>(singletonList(account3)));
        account2.setIncomingFriendRequests(new HashSet<>(singletonList(account1)));
        account3.setIncomingFriendRequests(new HashSet<>(asList(account1, account2)));

        account4.setCreatedGroups(new ArrayList<>(asList(group1, group2, group3)));
        account2.setModeratedGroups(new HashSet<>(singletonList(group3)));
        account3.setModeratedGroups(new HashSet<>(asList(group2, group3)));
        account1.setSubscriptions(new HashSet<>(asList(group2, group3)));
        account2.setSubscriptions(new HashSet<>(singletonList(group2)));
        account3.setSubscriptions(new HashSet<>(singletonList(group1)));
        account1.setRequestedGroups(new HashSet<>(singletonList(group1)));
        account2.setRequestedGroups(new HashSet<>(singletonList(group1)));

        group1.setModerators(new HashSet<>());
        group1.setSubscribers(new HashSet<>(singletonList(account3)));
        group1.setRequesters(new HashSet<>(asList(account1, account2)));
        group2.setModerators(new HashSet<>(singletonList(account3)));
        group2.setSubscribers(new HashSet<>(asList(account1, account2)));
        group2.setRequesters(new HashSet<>());
        group3.setModerators(new HashSet<>(asList(account2, account3)));
        group3.setSubscribers(new HashSet<>(singletonList(account1)));
        group3.setRequesters(new HashSet<>());
    }
}