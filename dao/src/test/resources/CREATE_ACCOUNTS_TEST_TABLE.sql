CREATE TABLE IF NOT EXISTS accounts
(
  user_id        BIGINT AUTO_INCREMENT,
  lastName       VARCHAR(128) NOT NULL,
  firstName      VARCHAR(128) NOT NULL,
  middleName     VARCHAR(128),
  birthDate      DATE,
  homeAddress    VARCHAR(128),
  workAddress    VARCHAR(128),
  email          VARCHAR(128) NOT NULL,
  icq            VARCHAR(128),
  skype          VARCHAR(128),
  additionalInfo VARCHAR(1024),
  password       VARCHAR(128) NOT NULL,
  avatar         MEDIUMBLOB,
  CONSTRAINT accounts_pk PRIMARY KEY (user_id),
  CONSTRAINT email_uq UNIQUE (email)
);