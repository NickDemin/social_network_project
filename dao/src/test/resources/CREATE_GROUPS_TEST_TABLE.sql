CREATE TABLE IF NOT EXISTS groups
(
  group_id    BIGINT AUTO_INCREMENT,
  group_name  VARCHAR(128) NOT NULL,
  description VARCHAR(1024),
  avatar      MEDIUMBLOB,
  creator_id  BIGINT       NOT NULL,
  CONSTRAINT group_pk PRIMARY KEY (group_id),
  CONSTRAINT group_creator_fk FOREIGN KEY (creator_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE,
  CONSTRAINT name_uq UNIQUE (group_name)
);

CREATE TABLE IF NOT EXISTS groups_moderators
(
  group_id     BIGINT NOT NULL,
  moderator_id BIGINT NOT NULL,

  CONSTRAINT groups_moderators_pk PRIMARY KEY (group_id, moderator_id),
  CONSTRAINT gm_group_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE,
  CONSTRAINT gm_moderator_fk FOREIGN KEY (moderator_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS groups_subscribers
(
  group_id      BIGINT NOT NULL,
  subscriber_id BIGINT NOT NULL,

  CONSTRAINT groups_subscribers_pk PRIMARY KEY (group_id, subscriber_id),
  CONSTRAINT gs_group_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE,
  CONSTRAINT gs_subscriber_fk FOREIGN KEY (subscriber_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS groups_requests
(
  group_id     BIGINT NOT NULL,
  requester_id BIGINT NOT NULL,

  CONSTRAINT groups_requests_pk PRIMARY KEY (group_id, requester_id),
  CONSTRAINT gr_group_fk FOREIGN KEY (group_id) REFERENCES groups (group_id)
    ON DELETE CASCADE,
  CONSTRAINT gr_requester_fk FOREIGN KEY (requester_id) REFERENCES accounts (user_id)
    ON DELETE CASCADE
);